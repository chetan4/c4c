require 'pp'
task :c4c_stats => :environment do
  puts "TT\t | RT \t | | TC \t| RT% \t |Name"
  SearchKeyword.all.each do |sk|
  keyword = sk.keyword
  in_reply = sk.tweets.select {|x|x.tweet_data["inReplyTo"].present?}.size
  total_search_tweets =  sk.tweets.select {|x|x.tweet_data["inReplyTo"].blank?}.size
  total = sk.tweets.parent_tweets.has_children.size
  percentage = (in_reply.to_f/total.to_f)*100.0 rescue 0
  puts "#{total_search_tweets} \t| #{in_reply} \t | #{total} \t | #{percentage} \t | #{keyword} "

    
  end
end