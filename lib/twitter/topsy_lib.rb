module TopsyLib
  
  def search_tweets(keywords,options={})
    client = TopsyApi::Client.new
    client.tweets(keywords,options)
  end

  def fetch_tweet(ids)
    client = TopsyApi::Client.new
    client.tweet(ids)
  end

  def fetch_conversation(url)
    client = TopsyApi::Client.new
    client.conversations(url)
  end
end