class TwitterLib
  def initialize(keywords = [])
    @keywords = keywords
    @conversation = nil
  end

  def get_tweets
    tweets = []
    @keywords.each do |k|
      tweets << C4C::TweetAnalyzer.new([k]).get_tweets
    end
    tweets.flatten
  end

  def get_conversation(tweet)
    @conversation = C4C::Conversation.new(tweet).get_conversation rescue []
    @conversation
  end
end
