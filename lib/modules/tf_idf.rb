module TfIdf
  class TfIdfScore
    def initialize(blob, string_blobs)
      @scores = {}
      @blob = blob || ''
      @string_blobs = string_blobs || []
      @scanned_words = scan_words(@blob)
    end

    def get_score
      word_frequency || {}
    end

    private
    def word_frequency
      @scores = {}
      @scanned_words.each do |word|
        @scores.merge!(word => tf_idf(word, @scanned_words, @string_blobs))
      end
      @scores
    end

    # term frequency. No of times a term occurs
    def tf(word, words)
      ("%1.5f" % [words.count(word).to_f / words.length.to_f]).to_f rescue 0
    end

    # term occuring how many times in blog
    # needs tweaking
    def n_containing(word, bloblist)
      sum = 0
      bloblist.map{|b| sum += scan_words(b).count(word)}
      sum
    end

    # inverse document frequency
    def idf(word, bloblist)
      (Math.log(bloblist.length) / (1 + n_containing(word, bloblist))) rescue 0
    end

    # calculate tf, idf
    def tf_idf(word, words, bloblist)
      (tf(word, words) * idf(word, bloblist))
    end

    def scan_words(string)
      return [] if string.empty?
      sanitize(string.downcase.scan(/[\w']+/))
    end

    def sanitize(input = [])
      if !input.empty?
        input.map{|i| i.downcase!}
        ((((input - STOP_WORDS
            ) - PUNCTUATION_MARKS
           ) - NUM
          ) - ALPHA).uniq.delete_if{|s| (s =~ /\d/ || s =~ /(.)\1+/)}
      end
    rescue Exception => e
      puts e.message
      puts e.backtrace
      return ['errr sanitize']
    end
  end
end
