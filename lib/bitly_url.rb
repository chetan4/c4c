class BitlyUrl
  def initialize(url)
    @url ||= url
    @bitly = Bitly.client
  end

  def short_url
    @bitly.shorten(@url).short_url rescue ''
  end

  def expand
    @bitly.expand(@url).long_url rescue ''
  end
end
