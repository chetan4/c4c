require 'uri'
module GnipLib

  module GnipUrl
    def search_url
      "https://search.gnip.com/accounts/IdyllicSoftware/search/prod.json"
    end 

    def rehydration_url
      "https://rehydration.gnip.com:443/accounts/IdyllicSoftware/publishers/twitter/rehydration/activities.json"
    end

    def search_count_url
      "https://search.gnip.com/accounts/IdyllicSoftware/search/prod/counts.json"
    end
  end
end