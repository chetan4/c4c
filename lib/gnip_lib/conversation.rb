require 'uri'
module GnipLib
  class Conversation
    extend GnipLib::GnipUrl
    attr_accessor :tweet_id,:response,:parent_tweet,:sub_tweets
    def initialize(tweet_id)
      @tweet_id = tweet_id

    end
    
    # For InReplyTo is not present
    def get_conversation_for_parent_tweet
      @response = GnipApi::Search.new("https://search.gnip.com/accounts/IdyllicSoftware/search/prod.json")
      begin
        query_rule  = "in_reply_to_status_id: #{tweet_id}"
        @response.search({:query=>query_rule,:publisher=>"twitter"})
      rescue
      end  
      @response
    end

    # For InReplyTo is present
    def get_conversation_for_reply_tweet
      parent_tweet_id,@parent_tweet,@sub_tweets = GnipLib::Conversation.fetch_parent_tweet_id(@tweet_id)
      # Some time top most paent has more than one month
    
      if @parent_tweet && @parent_tweet["available"] 
        @response = GnipApi::Search.new("https://search.gnip.com/accounts/IdyllicSoftware/search/prod.json")
        begin
          query_rule  = "in_reply_to_status_id: #{parent_tweet_id}"
          @response.search({:query=>query_rule,:publisher=>"twitter"})
        rescue
        end  
        @response
      else
        @response = nil
      end  

    end
    
    class << self

      def fetch_conversations_from_url(url)
        tweet_id =  url.split("/").last
        conversation = Conversation.new(tweet_id)
      end

      def fetch_conversations_from_tweet_id(tweet_id)
        conversation = Conversation.new(tweet_id)
      end

      def fetch_conversation(url_tweet_id)
        if url_tweet_id =~ URI::regexp
          fetch_conversations_from_url(url_tweet_id) 
        else
          fetch_conversations_from_tweet_id(url_tweet_id) 
        end  
      end

      # Call for rehydration API
      # It's  recursive call and find out array of last parents
      def fetch_parent_tweet_id(tweet_id)
        tweet = GnipLib::GnipTweet.new(tweet_id)
        tweet.get_parent_tweets
        parent_tweet_id = tweet.parent_tweet_ids[tweet_id]
        if tweet_id == parent_tweet_id
          [parent_tweet_id,nil,nil]
        else
          tree_tweets = tweet.build_tree_tweets_hash
          [parent_tweet_id,tree_tweets[parent_tweet_id],tree_tweets]
        end  

      end
   
    end  


  end
end