require 'uri'
module GnipLib
  class MultipleConversation
    extend GnipLib::GnipUrl
    attr_accessor :tweet_ids,:response,:gnip_tweet,:parent_tweet_ids
    # Max limit of tweet ids is 20
    def initialize(tweet_ids)
      @tweet_ids = Array(tweet_ids)
    end
    # For InReplyTo is not present
    def get_conversation_for_parent_tweets
      @response = GnipApi::Search.new("https://search.gnip.com/accounts/IdyllicSoftware/search/prod.json")
      @parent_tweet_ids = @tweet_ids
      if @tweet_ids
        query_rule  = Array(@tweet_ids).map  {|tweet_id| "in_reply_to_status_id : #{tweet_id}" }.join(" OR ")
        begin
          @response.search({:query=>query_rule,:publisher=>"twitter"})
        rescue
        end  
      end  
      @response
    end

    # For InReplyTo is  present
    def get_conversation_for_reply_tweets
      @parent_tweet_ids,@gnip_tweet = GnipLib::MultipleConversation.fetch_parent_tweet_ids(@tweet_ids)
      @response = GnipApi::Search.new("https://search.gnip.com/accounts/IdyllicSoftware/search/prod.json")
      if @parent_tweet_ids
        query_rule  = Array(@parent_tweet_ids).map  {|tweet_id| "in_reply_to_status_id : #{tweet_id}" }.join(" OR ")
        begin
          @response.search({:query=>query_rule,:publisher=>"twitter"})
        rescue
        end  
      end  
      @response
    end

    # FIRE Request to get final parent tweet ids for in reply to tweets
    def self.fetch_parent_tweet_ids(tweet_ids)
      tweet = GnipLib::GnipTweet.new(tweet_ids)
      tweet.get_parent_tweets
      tree_tweets = tweet.build_tree_tweets_hash
      parent_tweet_ids = tweet.parent_tweet_ids.select {|tweet_id,parent_id| tree_tweets[parent_id.to_s]["available"] rescue false}.values
      [parent_tweet_ids,tweet]
    end
    

  end
end