# Calling rehydration API
module GnipLib
  class GnipTweet
    extend GnipLib::GnipUrl
    attr_accessor :tweet_ids,:tweets,:response,:tree_tweets,:parent_tweet_ids,:parent_tweet_hash

    def initialize(tweet_ids)
      @tweet_ids = Array(tweet_ids)
      @response = GnipApi::Rehydration.new("https://rehydration.gnip.com:443/accounts/IdyllicSoftware/publishers/twitter/rehydration/activities.json")
      @tree_tweets = []
    end

    # fetch tweets based on ids
    def get_tweets
      @response.get_tweets(@tweet_ids)
      @tweets = @response.parse_response
    end

    # Fetch ALL parent tweets based on tweet_ids
    def get_parent_tweets
      get_tweets  unless  @tweets 
      @parent_tweet_hash = get_parents(self,@tweet_ids)
      @tree_tweets = @tree_tweets.flatten.uniq.compact
      @parent_tweet_ids = build_parent_tweets(@parent_tweet_hash)
    end

     # Build{:tweet_id=>Content}
    def build_tree_tweets_hash
      tweets = {}
      @tree_tweets.map {|tweet|tweets[tweet["id"]] = tweet} if @tree_tweets
      tweets
    end
    protected

    # fetch parent recursively 
    def get_parents(current_object,tweet_ids,parent_tweets={})
      in_reply_tweet_ids = []
      @tweets.each do |tweet|
        #  If tweet is available
        if tweet["available"]
          # If tweet has in reply to 
          if tweet["content"]["inReplyTo"]
            # Get Tweet ID from  Link
            in_reply_tweet_link = tweet["content"]["inReplyTo"]["link"]
            in_reply_tweet_id = in_reply_tweet_link.split("/").last rescue  nil
            in_reply_tweet_ids.push(in_reply_tweet_id)
            # tweet id hash is present then set new value
            if parent_tweets[tweet["id"]]
             parent_tweets[tweet["id"]] = in_reply_tweet_id
            else
              parent_tweets[tweet["id"]] = in_reply_tweet_id
            end

          else
            # If no InreplyTO found means it's last i.e its parent
             parent_tweets[tweet["id"]] = nil
          end  
        else
          # If no InreplyTO found means it's last i.e its parent
          # There many be chance tweet is older than one month gnip wont be able to find out  or deleted
            parent_tweets[tweet["id"]] = nil
        end
        # Call recursive
      end

      # DO this recursily find top parent
      if in_reply_tweet_ids.flatten.uniq.compact.blank?
        return parent_tweets
      else  
        children = GnipLib::GnipTweet.new(in_reply_tweet_ids.flatten.uniq.compact)
        current_object.tree_tweets.push(children.get_tweets)
        children.get_parents(current_object,children.tweet_ids,parent_tweets)
        return parent_tweets
     
      end  
    end

    # Build {Tweetid=>parent tweet}
    def build_parent_tweets(parent_tweets)
      parent_tweet_ids = {}
      @tweet_ids.each do |tweet_id|
        parent_tweet_ids[tweet_id.to_s]= build_parent_tweet_id(tweet_id.to_s,parent_tweets)
      end
      parent_tweet_ids
    end

    # Build {Tweetid=>parent tweetid}
    def build_parent_tweet_id(tweet_id,parent_tweets)
      if parent_tweets[tweet_id.to_s]
        build_parent_tweet_id(parent_tweets[tweet_id.to_s],parent_tweets)
      else
        return  tweet_id
      end
    end
   

    
  end
end  
