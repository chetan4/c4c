require 'gnip_api'
module GnipLib
  class Search
    # extend GnipLib::GnipUrl
    attr_accessor :keyword,:max_results,:from_date,:to_date,:response

    def initialize(keyword,options={})
      @keyword = keyword
      @max_results = options[:max_results] || 10
      @from_date = options[:from_date]  || Date.today - 30.days
      @to_date = options[:to_date] || Date.today
      @bucket = options[:bucket] || "day"
    end

    def search_tweets
      
      @response = GnipApi::Search.new("https://search.gnip.com/accounts/IdyllicSoftware/search/prod.json")
      begin
        @response.search({:query=>@keyword,:publisher=>"twitter",:maxResults=>@max_results,:fromDate=>format_date(@from_date),:toDate=>format_date(@to_date)})
      rescue
      end  
      @response

    end
    def count_tweets
      @response = GnipApi::Search.new("https://search.gnip.com/accounts/IdyllicSoftware/search/prod/counts.json")
      begin
        @response.count_tweets({:query=>@keyword,:publisher=>"twitter",:fromDate=>format_date(@from_date),:toDate=>format_date(@to_date),:bucket=>@bucket})
      rescue
      end  
      @response
    end
    
    def format_date(date)
      Time.parse(date.to_s).strftime("%Y%m%d%H%M")
    end

    def get_conversation_tweets
      mashie_response = Hashie::Mash.new(@response.parse_response)
      Array(mashie_response.results).select {|tweet|tweet.inReplyTo }
    end

    def get_conversation_tweet_ids
      conversation_tweets = get_conversation_tweets
      if conversation_tweets.present?
        reply_tweet_urls = conversation_tweets.collect{|tweet| tweet.inReplyTo.link}
        reply_tweet_ids = reply_tweet_urls.collect{|url| url.split("/").last}
      else
        []
      end
    end

    def get_converations
      conversation_tweets = get_conversation_tweets
      if conversation_tweets.present?
        reply_tweet_urls = conversation_tweets.collect{|tweet| tweet.inReplyTo.link}
        reply_tweet_ids = reply_tweet_urls.collect{|url| url.split("/").last}

      else
        []
      end
        
    end



  end
end