# -*- coding: utf-8 -*-
class FeedParser
  def initialize(url)
    @url = url || nil
    @feed = Feedzirra::Feed.fetch_and_parse(@url)
    @keywords_hash = {}
  end

  def title
    @feed.title
  end

  def url
    @url
  end

  def entries
    @feed.entries
  end

  def last_modified
    @feed.last_modified
  end

  def self.valid?(url)
    begin
      feed  = Feedzirra::Feed.fetch_and_parse(url.strip)
      feed.url.present? || feed.feed_url.presnt? || feed.title.present? || feed.entries.present?
    rescue
      false
    end  
  end
end
