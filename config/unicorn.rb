worker_processes 3 # amount of unicorn workers to spin up
timeout 240   
# preload_app true
before_fork do |server, worker|
  # ...
  defined?(ActiveRecord::Base) and ActiveRecord::Base.connection.disconnect!
   Signal.trap 'TERM' do
    puts 'Unicorn master intercepting TERM and sending myself QUIT instead'
    Process.kill 'QUIT', Process.pid
  end
end

after_fork do |server, worker|
  # ...
  defined?(ActiveRecord::Base) and ActiveRecord::Base.establish_connection
   Signal.trap 'TERM' do
    puts 'Unicorn worker intercepting TERM and doing nothing. Wait for master to sent QUIT'
  end
end