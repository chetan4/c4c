require 'sidekiq/web'
C4c::Application.routes.draw do

  mount RailsAdmin::Engine => '//rails_admin', as: 'rails_admin'
  resources :charges do
    collection do
      get 'payment_failed'
    end
  end

  get '/feeds/validate_feed' => 'feeds#validate_feed', as: :validate_feed
  # temp route for following conversations

  get '/tweets/fetch' => 'tweets#fetch'
  get '/search_tweets/:id' => 'feed_entries#search_tweets',:as=>"search_tweets"

  get '/auth/twitter', :as => :twitter_auth

  get "/mark_non_relavant"=> "tweets#mark_non_relavant",:as=>:mark_non_relavant
  get "/follow_conversation"=> "tweets#follow_conversation",:as=>:follow_conversation
  get "/unfollow_conversation"=> "tweets#unfollow_conversation",:as=>:unfollow_conversation
  get "/tweet_it"=> "tweets#tweet_it",:as=>:tweet_it
  get "/old_landing"=> "home#old_landing"
  get "/about_us"=> "home#about_us"
  get "/toc"=> "home#toc"
  get "/privacy-policy"=> "home#privacy_policy"
  get "/new_landing"=> "home#new_landing"

  resources :feed_entries do
    collection do
      get '/search' => 'feed_entries#search_blog_posts', :as => :search_blog_posts
      get '/fetch_new_articles' => 'feed_entries#fetch_new_articles', :as => :fetch_new_articles
      get '/fetch_tweets_and_email'=>'feed_entries#fetch_tweets_and_email',:as=>:fetch_tweets_and_email

    end
    member do
      get 'show_feed_tweets'
      get 'load_more_tweets'
      get 'load_more_tweets_for_keyword'
      get 'conversation_tweets'
      get 'following'
      get 'user_conversations'
      get 'remove_search_keyword'
      get 'add_search_keyword'
      get 'toggle_blog_important'
      get 'render_tweets'
      get 'render_fetch_tweet'
      get "update_last_seen"

    end
  end

  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"


  resources :feeds
  post '/feeds/import' => "feeds#import", :as => :feed_import
  get '/guest/sign_in' => "users#social_sign_in", :as => :social_sign_in

  # omniauth callbacks
  get '/auth/:provider/callback' => "users#social_sign_in"

  # update email address after twitter sign up
  post '/users/update_email' => "users#update_email", :as => :update_email
  post '/home/signup' => 'home#signup', as: :signup_import

  # deactivate duplicate feeds
  get '/feeds/deactivate/:id' => 'feeds#deactivate', as: :feed_deactivate
  get '/feeds/activate/:id' => 'feeds#activate', as: :feed_activate

  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
      username == 'admin' && password == 'admin123'
  end
  mount Sidekiq::Web => '/sidekiq_workers'


root 'home#index'
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
