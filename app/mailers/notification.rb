class Notification < ActionMailer::Base
  default from: "notifications@c4chq.com"
  def unseen_email(user_id)
    @user = User.find(user_id)
    @no_of_conversations = @user.unseen_conversations_count
    mail(to: "#{@user.email}",:cc=> 'jparekh@idyllic-software.com', subject: 'You have new conversations').deliver
  end
end
