class GnipStreamWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :gnip_stream, :backtrace => true,:retry => 2
  def perform(tweet_data)
    begin
      tweet_id = tweet_data["object"]["id"].split(":").last
      tweet = Tweet.where(:tweet_id=>tweet_id.to_i).first
      tweet = Tweet.new(:tweet_id=>tweet_id.to_i) if tweet.blank?
      tweet.tweet_data = tweet_data
      if tweet_data["inReplyTo"].present? && tweet_data["inReplyTo"]["link"].present?
        in_reply_tweet_id = tweet_data["inReplyTo"]["link"].split("/").last
        in_reply_tweet = Tweet.where(:tweet_id=>in_reply_tweet_id.to_i).first
        tweet.parent =  in_reply_tweet if in_reply_tweet
      end
      if tweet.save
        tweet.add_gnip_rule
      end
    rescue
    end  

    puts tweet
  end
end