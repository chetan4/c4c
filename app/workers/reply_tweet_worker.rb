class ReplyTweetWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :reply_tweet, :retry => 2, :backtrace => true
  def perform(reply_tweet_id)
    tweet = Tweet.find(reply_tweet_id).get_reply_tweet_from_gnip
  end
end