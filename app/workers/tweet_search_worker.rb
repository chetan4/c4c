class TweetSearchWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :tweet_search,:backtrace => true,:retry => 2
  def perform(search_keyword_id,feed_entry_id,try_count=0,total_conversations=0)
    begin
      search_keyword_id =  search_keyword_id["$oid"] if search_keyword_id.is_a?(Hash)
      feed_entry_id =  feed_entry_id["$oid"] if feed_entry_id.is_a?(Hash)
      feed_entry = FeedEntry.find(feed_entry_id)
      search_keyword = SearchKeyword.find(search_keyword_id)
      #  SEARCH TWEET
      if search_keyword.present?  && search_keyword.feed_entries.present? && search_keyword.feed_entries.select{|x|x.feed.active?}.size > 0
        search_keyword.search_tweet
        # Find Out Tweet which has not been processed

        search_tweets = search_keyword.tweets.parent_tweets.where(:tweet_status=>Tweet::NO_SEARCH)
        if search_tweets.present? 
          
          is_reply_conversation,reply_conversations = process_in_reply_tweets(search_tweets,feed_entry,search_keyword)
          # is_parent_conversation,parent_conversations = process_parent_tweets(search_tweets,feed_entry,search_keyword)
          # if((reply_conversation || parent_conversation) &&  try_count <= 7)
          #   TweetSearchWorker.new.perform(search_keyword.id.to_s,feed_entry_id,try_count+1)
          # end
          # JUST FOR TESTING

          total_conversations = total_conversations+reply_conversations.size#+parent_conversations.size
          puts "##################CONVERSTAION- #{total_conversations}------------"
          # Update Feed Entry Load more count if any
          
          # if  search_keyword.get_schedule.present?
          if  (total_conversations <= 60) && search_keyword.get_schedule.present?
            TweetSearchWorker.perform_async(search_keyword.id.to_s,feed_entry_id,try_count+1,total_conversations)
          end
          # elsif  try_count <= 5
        elsif search_keyword.get_schedule.present?
          TweetSearchWorker.perform_async(search_keyword.id.to_s,feed_entry_id,try_count+1,0)
          # TweetSearchWorker.new.perform(search_keyword.id.to_s,feed_entry_id,try_count+1)
        end
      end  
    rescue
     
    ensure
      
      search_keyword.feed_entries.where("load_more" =>true).each do |f|
        f.update_load_more_count
        f.update_searching
      end  
    end
  end

  def process_in_reply_tweets(search_tweets,feed_entry,search_keyword)
    #  Find Out Reply Tweet
    reply_tweets = search_tweets.select {|tweet| tweet.tweet_data["inReplyTo"]}
    #  Set Conversation Tweet to false
    is_conversation =false
    conversions = []
    # Crate Batch of 15 reply tweet and process them
    reply_tweets.each_slice(30).to_a.each do |reply_tweet|
      begin
        # if conversions.size < 5
        conversion_tweet_ids = Tweet.fetch_multi_conversation_for_in_reply_to(reply_tweet.collect{|x| x.id.to_s})
        #  IF Tweeet is found fetch it and send it to
        if conversion_tweet_ids.present?
          conversion_tweets  = Tweet.where(:id.in=>conversion_tweet_ids)
          if  conversion_tweets.length > 0 && feed_entry.search_keywords.include?(search_keyword)
            conversions << conversion_tweets
            is_conversation = true
            # Send to private pub to render
            PrivatePub.publish_to "/tweets/#{feed_entry.id.to_s}", :tweet_ids => conversion_tweets.collect{|t|t.id.to_s},:search_keyword_id=> search_keyword.id.to_s
          end
        end
        # end
      rescue Exception=>e
      end
    end
    [is_conversation,conversions.flatten.uniq.compact]
  end



  def process_parent_tweets(search_tweets,feed_entry,search_keyword)
    parent_tweets = search_tweets.select {|tweet| tweet.tweet_data["inReplyTo"].blank?}
    is_conversation =false
    conversions = []
    parent_tweets.each_slice(15).to_a.each do |parent_tweet|
      begin
        # if conversions.size < 5
          conversion_tweet_ids = Tweet.fetch_multi_conversation_for_parent(parent_tweet.collect{|x| x.id.to_s})
          if conversion_tweet_ids.present?

            conversion_tweets  = Tweet.where(:id.in=>conversion_tweet_ids)

            if  conversion_tweets.length > 0 && feed_entry.search_keywords.include?(search_keyword)
              conversions << conversion_tweets
              is_conversation = true
              # Send to private pub to render
              PrivatePub.publish_to "/tweets/#{feed_entry.id.to_s}", :tweet_ids => conversion_tweets.collect{|t|t.id.to_s},:search_keyword_id=> search_keyword.id.to_s
            end
          end
        # end
      rescue
      end
      [is_conversation,conversions.flatten.uniq.compact]
    end
  end
end
