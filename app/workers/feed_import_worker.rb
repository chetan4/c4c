class FeedImportWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :feed_import, :retry => 2, :backtrace => true
  
  def perform(url, user_id)
    param = {
      :url => url,
      :user_id => user_id
    }
    Feed.new.import(param)
  end
end
