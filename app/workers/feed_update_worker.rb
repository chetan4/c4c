class FeedUpdateWorker
  include Sidekiq::Worker
  # include Sidetiq::Schedulable
  sidekiq_options :queue => :feed_update, :retry => 2, :backtrace => true
  # recurrence { daily }

  def perform
    Feed.all.each do |feed|
      feed.add_feed_entries
    end  
  end
end
