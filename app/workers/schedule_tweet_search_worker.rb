# Used For 
class ScheduleTweetSearchWorker
  include Sidekiq::Worker
  # include Sidetiq::Schedulable
  sidekiq_options :queue => :schedulable_tweet_search,
  :backtrace => true, :retry => 2

  # recurrence {daily}

  def perform
   
    search_keywords = SearchKeyword.all.to_a
    search_keywords.each do |search_keyword|
      begin
        if search_keyword.feed_entries.present? && search_keyword.feed_entries.select{|x|x.feed.active?}.size > 0
          search_keyword.search_tweet
          search_tweets = search_keyword.tweets.parent_tweets.where(:tweet_status=>Tweet::NO_SEARCH).where(:created_at.gte=>(DateTime.now - 30.days))
          process_in_reply_tweets(search_tweets,search_keyword) if search_tweets.present?
        end  
      rescue
      end
    end
    UnseenEmailWorker.perform_async
  end
  def process_in_reply_tweets(search_tweets,search_keyword)
    #  Find Out Reply Tweet
    reply_tweets = search_tweets.select {|tweet| tweet.tweet_data["inReplyTo"]}
    #  Set Conversation Tweet to false
    # Crate Batch of 15 reply tweet and process them
    reply_tweets.each_slice(30).to_a.each do |reply_tweet|
      begin
        Tweet.fetch_multi_conversation_for_in_reply_to(reply_tweet.collect{|x| x.id.to_s})
      rescue Exception=>e
      end
    end
  end
  
end
