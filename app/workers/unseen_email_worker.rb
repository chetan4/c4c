class UnseenEmailWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable
  sidekiq_options :queue => :unseen_email_worker,:backtrace => true, :retry => 2
  # recurrence {daily}
  def perform
    users = User.all.to_a
    users.each do |user|
      Notification.unseen_email(user.id.to_s) if user.unseen_conversations_count > 0
    end
    
  end
end
