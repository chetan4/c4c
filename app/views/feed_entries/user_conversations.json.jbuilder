json.feed_entry_id @feed_entry.id
json.search_tweets do 
  json.array! @user_conversations.each do |user_conversation|
    json.partial! 'feed_entries/tweet', tweet: user_conversation.tweet
  end
end  

