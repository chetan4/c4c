json.tweet do
  json.tweet_id tweet.id
  json.tweet_data tweet.tweet_data
  json.reply_count tweet.replies.size
  search_keywords = tweet.get_parent.search_keywords if search_keywords.blank?
  json.search_keywords Array(search_keywords)

  json.replies do
    json.array! tweet.replies.to_a.each  do |reply|
    #json.array! tweet.descendants.each  do |reply|

     # json.partial! 'feed_entries/tweet', tweet: reply ,search_keywords:  search_keywords
      json.tweet do
        json.tweet_id reply.id
        json.tweet_data reply.tweet_data
        json.reply_count reply.replies.size
        json.search_keywords Array(search_keywords)
      end  
    end  
  end
    
end