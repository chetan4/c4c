
  #json.array!  @feed_entry.search_keywords do |search_keyword|
  #  json.keyword search_keyword.keyword
    json.feed_entry_id @feed_entry.id
    
    json.search_keyword @search_keyword
    json.search_tweets do 
      #json.array! search_keyword.tweets.parent_tweets.relavant_tweets.not_in(:_id=>current_user.#user_conversations.collect{|x| x.tweet.id}) do |parent_tweet|
      
      #json.array! search_keyword.tweets.parent_tweets.has_children.each do |parent_tweet|
      
      json.array! @tweets.each do |tweet|

        json.tweet do
          json.tweet_id tweet.id
          json.tweet_data tweet.tweet_data
          json.reply_count tweet.replies.size
          unseen = tweet.created_at.to_i >= @feed_entry.last_seen_at.to_i rescue false
          json.is_unseen unseen
          search_keywords = tweet.get_parent.search_keywords.without(:tweet_ids).to_a if search_keywords.blank?
          json.search_keywords do
            json.array! search_keywords.each do |k|
              json.(k,:id,:_id,:keyword,:feed_entry_ids)
            end
          end
          

          json.replies do
            json.array! tweet.replies.to_a.each  do |reply|
            #json.array! tweet.descendants.each  do |reply|

             # json.partial! 'feed_entries/tweet', tweet: reply ,search_keywords:  search_keywords
              json.tweet do
                json.tweet_id reply.id
                json.tweet_data reply.tweet_data
                json.reply_count reply.replies.size
                json.search_keywords do
                  json.array! search_keywords.each do |k|
                    json.(k,:id,:_id,:keyword,:feed_entry_ids)
                  end
                end
              end  
            end  
          end
            
        end
       # json.partial! 'feed_entries/tweet', tweet: tweet,:search_keywords=>@search_keyword
      end
    end  
    
 # end

