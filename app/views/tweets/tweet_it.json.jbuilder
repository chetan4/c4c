json.success true
json.feed_entry_id @feed_entry.id
json.parent_id @tweet.get_parent.id
json.partial! 'feed_entries/tweet', tweet: @tweet.get_parent
