class ChargesController < ApplicationController

before_filter :user_is_authenticated!, only: [:new, :create]


  def new
    @trial_ended = true unless (Date.today < (current_user.created_at + 7.days))
  end

  def create
    customer = Stripe::Customer.create(
      :email => params[:stripeEmail],
      :plan => 'starter',
      :card  => params[:stripeToken]
    )

    current_user.email = customer['email']
    current_user.stripe_id = customer['id']
    current_user.save!
    redirect_to feed_entries_path

  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to charges_path
  end

end
