class TweetsController < ApplicationController
  # fetch tweet for feed entry
  def fetch
    # keywords = FeedEntry.find(params[:id]).categories rescue []
    # tweets = []
    # keywords.each do |k|
    #   twitter = TwitterLib.new([k])
    #   tweets << twitter.get_tweets
    # end
    # tweets.flatten!
    # Tweet.save(tweets, current_user.id.to_s)
    # respond_to do |format|
    #   format.json {
    #     render :json => Tweet.build_tweets(tweets).to_json
    #   }
    # end
  end

  def mark_non_relavant
    @tweet = Tweet.find(params[:id])
    if @tweet.mark_non_relavant
      render :json=>{:success=>true}
    else 
      render :json=>{:success=>false}
    end
  end
  # 
  def follow_conversation
    if current_user.follow_conversation(params[:tweet_id],params[:feed_entry_id])
      render :json=>{:success=>true}
    else
      render :json=>{:success=>false}
    end
    
  end
  def unfollow_conversation
    if current_user.unfollow_conversation(params[:tweet_id],params[:feed_entry_id])
      render :json=>{:success=>true}
    else
      render :json=>{:success=>false}
    end
    
  end

  def tweet_it
    @tweet = Tweet.find(params[:tweet_id])
    @feed_entry = FeedEntry.find(params[:feed_entry_id])
    reply_tweet,success = @tweet.in_reply_tweet(current_user,params[:tweet_text])
    if success 
      current_user.follow_conversation(@tweet.get_parent.id,@feed_entry.id,@tweet.id,reply_tweet.id)
      if reply_tweet.tweet_data.present?
        
      else  
        render :json=>{:success=>false,:parent_id=>@tweet.get_parent.id.to_s,:tweet_id=>@tweet.id.to_s,:tweet_text=>params[:tweet_text]}
        # current_user.follow_conversation(@tweet.get_parent.id,@feed_entry.id)
      end
    else      
      render :json=>{:success=>false,:message=>reply_tweet.to_s,:tweet_id=>@tweet.id.to_s}
    end

  end
end
