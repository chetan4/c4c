class HomeController < ApplicationController
  http_basic_authenticate_with name: "idyllic", password: "p@ssw0rd"

  before_filter :redirect_if_signed_in,:except=>[:about_us,:toc,:privacy_policy]
  def index
    @new_user = session[:new_user]
    render 'index', layout: false
  end

  def old_landing
    render layout: false
  end

  def new_landing
    render layout: false
  end

  def signup
    session[:blog_url] = params[:url] if !params[:url].empty?
    redirect_to twitter_auth_path, method: :get
  end

  def redirect_if_signed_in
    if user_signed_in?
      redirect_to feed_entries_path
    end
  end
end
