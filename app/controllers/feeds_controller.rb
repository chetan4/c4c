class FeedsController < ApplicationController

  

  # GET /feeds
  def index
    @feeds = Feed.where(:user_id => current_user.id)
  end

  # GET /feeds/1
  def show
    # @feed = Feed.where(:active => true)
    # @feed_entries = @feed.feed_entries
  end

  # GET /feeds/new
  def new
    @feed = Feed.new
  end

  # GET /feeds/1/edit
  def edit
  end

  # POST /feeds
  def create
    params[:feed][:user_id] = current_user.id
    @feed = current_user.feeds.new.import(params[:feed])
    if @feed.present?
      redirect_to feed_entries_path, notice: 'Feed was successfully created.'
    else  
      redirect_to feed_entries_path

    end
  end

  # PATCH/PUT /feeds/1
  def update
    if @feed.update(feed_params)
      redirect_to @feed, notice: 'Feed was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /feeds/1
  def destroy
    @feed.destroy
    redirect_to feeds_url, notice: 'Feed was successfully destroyed.'
  end

  def deactivate
    Feed.find(params[:id]).update_attribute(:active, false)
    redirect_to feeds_path
  end
  def activate
    Feed.find(params[:id]).update_attribute(:active, true)
    redirect_to feeds_path
  end

  def validate_feed

    respond_to do |format|
      format.json{
        render json: FeedParser.valid?(params['url'].strip)
      }
    end
  end
end
