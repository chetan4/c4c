class UsersController < ApplicationController
  protect_from_forgery with: :null_session

  def social_sign_in
    @user = User.fetch_user(request.env['omniauth.auth'])
    if session[:blog_url].present?
      import_feed(session[:blog_url], @user.id.to_s)
      session.delete(:blog_url)
    end
    if @user.email.blank?
      @user.save(:validate=>false)
    end  
      sign_in @user if @user
    # else
    #   session[:new_user] = true
    #   info = request.env['omniauth.auth'].info.to_hash
    #   info.merge!(:provider => request.env['omniauth.auth'].provider,
    #               :uid => request.env['omniauth.auth'].uid)
    #   session[:user_auth_info] = info
    # end
    redirect_to feed_entries_path
  end

  def update_email
    
    if current_user.update_attributes(:email=>params[:email])
      if session[:blog_url].present?
        FeedImportWorker.perform_async(session[:blog_url], @user.id.to_s)
        session.delete(:blog_url)
      end
      redirect_to feed_entries_path  
    else  
      redirect_to feed_entries_path,:error=>"Please enter valid email"
    end  
  end
end
