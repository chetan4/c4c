class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  def import_feed(url, user_id)
    param = {
      :url => url,
      :user_id => user_id
    }
    Feed.new.import(param)
  end

  
  def add_email_id
    if current_user 
      redirect_to feed_entries_path if current_user.email.blank?
    end
  end
  protected
  def user_is_authenticated!
    redirect_to root_path unless current_user
    
  end

  def check_stripe_info
    # redirect_to new_charge_path unless current_user.stripe_id.present? or Date.today < (current_user.created_at + 7.days)  
  end
end
