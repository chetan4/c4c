class FeedEntriesController < ApplicationController

  before_filter :add_email_id,:except=>[:index]
  before_filter :user_is_authenticated!
  before_filter :get_feed, only: [:index, :search_blog_posts, :fetch_new_articles]
  before_filter :check_stripe_info
  
  def index
    if !@feed.nil?
      order = [[:important, :desc], [:published, :desc]]
      @feed_entries = []
      case params[:filter]
      when 'oldest'
        order = [[:published, :asc]]
        @feed_entries = @feed.feed_entries.order_by(order).page params[:page]
      when 'important'
        order = [[:important, :desc], [:published, :desc]]
        @feed_entries = @feed.feed_entries.order_by(order).page params[:page]
      when 'conversations'
        order = [['feed_entry.user_conversations.count', :desc], [:published, :desc]]
        list = @feed.feed_entries.includes(:user_conversations).sort_by{ |entry| entry.user_conversations.count}
        @feed_entries = Kaminari.paginate_array(list.reverse).page params[:page]
      else
        order = [[:published, :desc]]
        @feed_entries = @feed.feed_entries.order_by(order).page params[:page]
      end 
    end
  end

  def show
    @feed_entry = FeedEntry.find(params[:id])
    @tweets =  @feed_entry.relavant_tweets
    @user_converations = current_user.get_feed_entry_conversation(@feed_entry.id)
    
  end

  def new
    @feed_entry = FeedEntry.new
  end

  def edit
  end

  def create
    @feed_entry = FeedEntry.new(feed_entry_params)

    if @feed_entry.save
      redirect_to @feed_entry, notice: 'Feed entry was successfully created.'
    else
      render action: 'new'
    end
  end

  def update
    if @feed_entry.update(feed_entry_params)
      redirect_to @feed_entry, notice: 'Feed entry was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @feed_entry.destroy
    redirect_to feed_entries_url, notice: 'Feed entry was successfully destroyed.'
  end

  # temporary action for followed conversations
  def following
    @feed_entry = FeedEntry.find(params[:id])
    @user_converations = current_user.get_feed_entry_conversation(@feed_entry.id)
  end

  def search_tweets
    @feed_entry = FeedEntry.find(params[:id])

     @feed_entry.search_by_keywords(params[:keywords])
     # @feed_entry.start_searching
     
    # Considering one kwey word
    
    @search_keyword =  SearchKeyword.where(:keyword=>params[:keywords].downcase.strip).first
     
    #  If existing key word is added
    if @search_keyword && @search_keyword.tweets.parent_tweets.has_children.relavant_tweets.present?
      @tweets =  @search_keyword.tweets.parent_tweets.has_children.relavant_tweets
      render "search_tweets"
    else
      render :json=>{:success=>true}
    end
  end

  def load_more_tweets
    @feed_entry = FeedEntry.find(params[:id])
    @tweets = @feed_entry.search_by_keywords(@feed_entry.search_keywords.pluck(:keyword).join(","))
    @feed_entry.start_load_more
    render :json=>{:success=>true}
    #render "search_tweets"
  end

  def load_more_tweets_for_keyword
    if params[:keyword_id]
      @feed_entry = FeedEntry.find(params[:id])
      @search_keyword = SearchKeyword.find(params[:keyword_id])
      @tweets = @feed_entry.search_by_keywords(@search_keyword.keyword)
    end  

    render :json=>{:success=>true}
    # render "search_tweets"
  end

  def show_feed_tweets
    @feed_entry = FeedEntry.find(params[:id])
    @tweets =  @feed_entry.relavant_tweets
    render "search_tweets"
  end

  def conversation_tweets
    @feed_entry = FeedEntry.find(params[:id])
    @conversation_tweets = current_user.user_conversations.where(:feed_entry_id=>@feed_entry.id).collect{|x|x.tweet}.flatten.compact.uniq
    
    if @conversation_tweets.present?
      render :json=>{:tweets=>@conversation_tweets,:tweet_ids=>@conversation_tweets.collect{|x| x.id}}
    else
      render :json=>{:tweets=>[],:tweet_ids=>[]}
    end
  end

  def user_conversations
    @feed_entry = FeedEntry.find(params[:id])
    @user_conversations = current_user.get_feed_entry_conversation(@feed_entry.id)
  
  end

  def remove_search_keyword
    @feed_entry = FeedEntry.find(params[:id])
    @search_keyword = @feed_entry.search_keywords.find(params[:keyword_id])
    if @feed_entry.remove_search_keyword(@search_keyword) && @search_keyword.remove_feed_entry(@feed_entry)
      render :json=>{:success=>true}
    else
      render :json=>{:success=>false}
    end
  end

  def add_search_keyword
    @feed_entry = FeedEntry.find(params[:id])
    @keyword = @feed_entry.add_search_keyword(params[:keywords])
  end

  def search_blog_posts
    @feed_entries = @feed.feed_entries.where(:title => /.*#{params[:search]}.*/i) if !@feed.nil?
  rescue
    @feed_entries = nil
  end

  def fetch_new_articles
    @feed_entries = @feed.add_feed_entries
  end

  def toggle_blog_important
    @feed_entry = FeedEntry.find(params[:id])
    @feed_entry.important = !@feed_entry.important
    if @feed_entry.save
      render :json=>{:success=>true}
    else
      render :json=>{:success=>false}
    end
  end

  def render_tweets
    
    @feed_entry = FeedEntry.find(params[:id])
    @tweets = Tweet.where(:id.in=>params[:tweet_ids])
    @search_keyword = SearchKeyword.find(params[:search_keyword_id])
    render "search_tweets"
  end
  # Single tweet
  def render_fetch_tweet
    @feed_entry = FeedEntry.find(params[:id])
    
    @tweets = Tweet.where(:id.in=>params[:tweet_ids])
    
    
    render "search_tweets"
  end
  def update_last_seen
    @feed_entry = FeedEntry.find(params[:id])
    @feed_entry.update_last_seen
    render :json=>{:success=>true}
  end

  def fetch_tweets_and_email
    ScheduleTweetSearchWorker.perform_async
    redirect_to root_url
  end
  private
  def get_feed
    feeds = Feed.where(:user_id => current_user.id, :active => true)
    @feed = feeds.first
  end
end
