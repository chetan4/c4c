module ApplicationHelper
  def user_name
    (current_user.twitter_user_info["name"]  || "@"+current_user.twitter_user_info["screen_name"]) rescue ""
  end
end
