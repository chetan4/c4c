module FeedEntriesHelper
  def entries
    !@feed_entries.nil? ? @feed_entries : []
  end

  def feed_entry_keywords
    return [] if @feed_entry.keywords.nil?
    @feed_entry.keywords
  end

  def screen_name(t)
    t.user.screen_name.to_s rescue ''
  end

  def tweet_text(t)
    t.text rescue ''
  end

  def posted_on(t)
    '10 days ago'
  end

  def hash_tag(t)
    "ruby"
  end
end
