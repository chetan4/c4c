class SearchTime
  include Mongoid::Document
  include Mongoid::Timestamps
  field :mintime, type:DateTime
  field :maxtime, type:DateTime
  field :no_of_tweets,type: Integer,:default=>0
  field :more_results ,type:Boolean,:default=>false
  field :last_offset ,type:Integer
  field :influence_more_results  ,type:Boolean,:default=>false
  field :influence_last_offset ,type:Integer
  field :search_count,type:Integer,:default=>0
  has_and_belongs_to_many :tweets
  belongs_to :search_keyword
  scope :between_time, lambda{|schedule_time|lte(:mintime=>schedule_time).gte(:maxtime=>schedule_time)}

end
