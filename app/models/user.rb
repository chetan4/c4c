class User
  include Mongoid::Document
  include Mongoid::Timestamps

  has_many :feed_entries, dependent: :destroy
  has_many :feeds, dependent: :destroy
  has_many :user_conversations

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable

  field :guest, :type => Boolean

  ## Database authenticatable
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""

  # validates_presence_of :email, unless: :guest?
  # validates_presence_of :encrypted_password, allow_blank: true

  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  ## Confirmable
  # field :confirmation_token,   :type => String
  # field :confirmed_at,         :type => Time
  # field :confirmation_sent_at, :type => Time
  # field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  # field :locked_at,       :type => Time

  ## Token authenticatable
  # field :authentication_token, :type => String
  # run 'rake db:mongoid:create_indexes' to create indexes
  index({ email: 1 }, { unique: false, background: true })
  field :name, :type => String
  field :location,:type=>String
  validates_format_of :email, :with  => Devise.email_regexp
  # validates :email,  :email => true
  # validates_presence_of :name

  # omniauth fields
  field :uid, :type => String
  field :provider, :type => String # refactor later
  field :access_token, :type => String # refactor later
  field :access_secret, :type => String # refactor later
  field :twitter_user_info
  field :stripe_id
  field :payment_status


  def self.new_guest
    new {|u| u.guest = true}
  end

  def name
    guest ? "Guest" : email.to_s
  end

  def self.fetch_user(auth)

    return nil if auth.nil?
    user = User.where(:uid => auth.uid).first
    if user && user.access_secret.blank?
      user.access_secret = auth.credentials.secret
      user.access_token = auth.credentials.token

      user.save
    end
    if  user && user.twitter_user_info.blank?
      user.twitter_user_info = auth.info.merge(auth.extra["raw_info"])
    end
    return user if user
    user = User.new(:uid => auth.uid, :provider => auth.provider)
    user.access_secret = auth.credentials.secret
    user.access_token = auth.credentials.token

    user.twitter_user_info = auth.info.merge(auth.extra["raw_info"])
    user.name = user.twitter_user_info["name"]
    user
  end

  def follow_conversation(tweet_id,feed_entry_id,tweet_reply_id=nil,replied_tweet_id=nil)
    conversation = self.user_conversations.where(:feed_entry_id=>feed_entry_id,:tweet_id=>tweet_id).last
    if conversation.blank?
      conversation = self.user_conversations.new
      conversation.feed_entry = FeedEntry.find(feed_entry_id)
      conversation.tweet = Tweet.find(tweet_id)
    end
    conversation.add_reply(tweet_reply_id,replied_tweet_id) if tweet_reply_id && replied_tweet_id
    conversation.save
  
  end

  def unfollow_conversation(tweet_id,feed_entry_id)
    conversation = self.user_conversations.where(:tweet_id=>tweet_id).where(:feed_entry_id=>feed_entry_id)
    conversation.destroy
  end

  def get_feed_entry_conversation(feed_entry_id)
     user_conversations.where(:feed_entry_id=>feed_entry_id)
  end

  
  def twitter_name
   twitter_user_info["screen_name"] ||twitter_user_info["name"]
  end

  def unseen_conversations_count
    self.feed_entries.sum {|x|x.unseen_tweets.size}
  end

  def get_twitter_client
    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = TWITTER_CONSUMER_KEY
      config.consumer_secret     = TWITTER_CONSUMER_SECRET
      config.access_token        = self.access_token
      config.access_token_secret =  self.access_secret
    end
  end


end
