class UserReplyTweet
  include Mongoid::Document
  include Mongoid::Timestamps
  field :text
  field :reply_tweet_id
  belongs_to :user_conversation
  belongs_to :replied_tweet,class_name: 'Tweet', foreign_key: 'reply_tweet_id'
  belongs_to :tweet
end
