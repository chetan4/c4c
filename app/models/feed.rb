class Feed
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title, type: String
  field :url, type: String
  field :bitly_url, type: String
  field :active, type: Boolean

  has_many :feed_entries,:dependent=>:destroy
  belongs_to :user

  validates_presence_of :user_id
  validates_presence_of :url
  validates_presence_of :active

  def import(params)

    if FeedParser.valid?(params[:url])
      parsed_feed = FeedParser.new(params[:url])
      save_(parsed_feed, params[:user_id])
    else
      nil
    end  
  end

  # def add_feed_entry(feed_entry, user_id)
  #   self.feed_entries.create!(feed_entry_hash(e, user_id))
  # end
  def add_feed_entries
    feed_entry_blogs = self.feed_entries.pluck(:title)
    new_feeds = []
    parsed_feed = FeedParser.new(self.url)
    new_entries = parsed_feed.entries.select{|x|!feed_entry_blogs.include?(x.title.to_s)}
    new_entries.each do |feed_entry|
        new_feeds << self.feed_entries.create!(feed_entry_hash(feed_entry, self.user_id))
    end
    new_feeds
  rescue => e
    Rails.logger.error e  
    return []
  end

  private
  def save_(feed, user_id = nil)
    self.title = feed.title.to_s rescue nil
    self.url = feed.url.to_s
    self.bitly_url = BitlyUrl.new(feed.url.to_s).short_url rescue nil
    self.user_id = user_id.to_s
    self.active = true

    if self.save
      feed.entries.each do |e|
        self.feed_entries.create!(feed_entry_hash(e, user_id))
      end
      self
    else
      logger.info(" ====ERROR==== ")
      logger.info(self.errors.full_messages)
      return nil
    end
  rescue Exception => e
    logger.info(e.message)
    logger.info(e.backtrace)
    nil
  end

  def feed_entry_hash(e, user_id)
    {
      :title => (e.title.to_s rescue nil),
      :url => (e.url.to_s rescue nil),
      :author => (e.author.to_s rescue nil),
      :summary => (e.summary.to_s rescue nil),
      :content => (e.content.to_s rescue nil),
      :published => (e.published rescue nil),
      :categories => (e.categories rescue []),
      :user_id => user_id
    }
  end

 
end
