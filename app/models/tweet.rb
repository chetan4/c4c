class Tweet
  include Mongoid::Document
  include Mongoid::Timestamps
  extend TopsyLib
  # 0 means not search,1 means no childeren 2 means has children 3 means processing by background worker
  # 4 means background worker failed
  NO_SEARCH = 0
  NO_CHILDREN = 1
  HAS_CHILDREN = 2
  CONVERSATION_PROCESS = 3
  CONVERSATION_FAIL = 4
  RESPONSE_PARSE_ERROR = 5
  # tweet obj to_hash as is
  field :tweet_data, type: Hash
  field :parent_id
  field :tweet_id
  field :not_relavant,type: Boolean,:default=>false
  field :influence_tweet,type: Boolean,:default=>false
  field :tweet_status,type: Integer,:default=>0

  
  has_many :replies, class_name: 'Tweet', foreign_key: 'parent_id'
  belongs_to :parent, class_name: 'Tweet'
  
  has_and_belongs_to_many :search_keywords
  has_and_belongs_to_many :search_times
  has_many :user_conversations
  has_many :user_reply_tweets
  
  validates_presence_of :tweet_data
  validates_presence_of :tweet_id
  validates_uniqueness_of :tweet_id

  scope :parent_tweets,lambda{where(:parent_id.exists =>false)}
  scope :relavant_tweets,lambda{where(:not_relavant=>false)}
  scope :influence_tweets,lambda{where(:influence_tweet=>true)}
  scope :has_children,lambda{where(:tweet_status =>Tweet::HAS_CHILDREN)}
  # Pass date time  and created_at should be greater than last_seen_at
  scope :unseen_tweets,lambda{|last_seen_at|where(:created_at.gte =>last_seen_at.try(:to_time)) if last_seen_at.present?}

  index({ tweet_id: 1 }, { unique: true, name: "tweet_index" ,background: true })
  index({ parent_id: 1 }, {name: "parent_tweet_index" ,background: true })

  def get_conversation
    conversation = GnipLib::Conversation.new(self.tweet_id)
    tweet_info = get_tweet
    # IF In Reply TO present
    if tweet_info.inReplyTo
      conversation.get_conversation_for_reply_tweet
      # iF parent twet found
      if conversation.parent_tweet.present?
        tweet_id = conversation.parent_tweet["content"]["object"]["id"].split(":").last
        # find out create parent tweet
        tweet = Tweet.where(:tweet_id=>tweet_id).first
        tweet = Tweet.create(:tweet_id=>tweet_id,:tweet_data=> conversation.parent_tweet["content"]) if tweet.blank?
        # Apply Seach Kwyword of current tweet
        tweet.search_keywords  = ([tweet.search_keywords] + [self.search_keywords]).flatten.compact.uniq
        # Set Conversation
        tweet.set_conversaton(conversation)
      end
    else
      # Fetch Conversation for current tweet id
      conversation.get_conversation_for_parent_tweet
      set_conversaton(conversation)
    end
    
  end

  def descendants(decendants_tweets=[])
    replies = self.replies.to_a
    if replies.present?
      decendants_tweets << replies
      replies.each do |reply|
        if reply.replies.present?
          reply.decendants(decendants_tweets) 
        end
      end
    end  
    return decendants_tweets.flatten.compact.uniq
  end

  def set_conversaton(conversation)
    results = conversation.response.parse_response["results"]
    # If results os there
    if results.present?
      results.each do |result|
        tweet_id = result["object"]["id"].split(":").last
        # get tweet from db or create tweet and update parent id as current tweet
        tweet = Tweet.where(:tweet_id=>tweet_id).first
        if tweet.blank?
          tweet = Tweet.create(:tweet_id=>tweet_id,:tweet_data=>result,:parent_id=>self.id) 
        else
          tweet.update_attributes(:parent_id=>self.id)  
        end  
      end  
      # Result is present means current tweet has children
      self.tweet_status = HAS_CHILDREN 
    else
      # Result is empty means current tweet has no children 
      self.tweet_status = NO_CHILDREN
    end 
    self.save  
  end

  def tweet_url
    tweet = get_tweet
    tweet.link
  end

  def get_tweet
   tweet = Hashie::Mash.new(self.tweet_data)
  end

  def mark_non_relavant
    self.not_relavant = true
    self.save
  end

  def get_parent
    if self.parent
      self.parent.get_parent
    else  
      self
    end  
  end

  def get_all_replies(reply_tweets=[])
    
    if self.replies.present?
      reply_tweets << self.replies
      self.replies.each do |reply_tweet|
        reply_tweet.get_all_replies(reply_tweets)
        return reply_tweets.flatten.compact.uniq
      end
    else
      return reply_tweets.flatten.compact.uniq  
    end 

  end

  def add_gnip_rule
    begin
      rule = get_rule_connection
      rule.build_rule("in_reply_to_status_id:#{self.tweet_id}","#{self.tweet_id}")
      rule.add_rules
    rescue 
    end  
  end
  def remove_gnip_rule
     begin
      rule = get_rule_connection
      rule.build_rule("in_reply_to_status_id:#{self.tweet_id}","#{self.tweet_id}")
      rule.delete_reules
    rescue 
    end  
  end

  def in_reply_tweet(user,tweet_text)

    client = user.get_twitter_client
    
    begin
      tweet = client.update(tweet_text.to_s.truncate(140),:in_reply_to_status_id=>self.tweet_id)
      if tweet.id.present?
        reply_tweet = Tweet.new(:tweet_id=>tweet.id)
        reply_tweet.parent = self
        reply_tweet.save(:validate=>false)
        reply_tweet.get_reply_tweet_from_gnip
        [reply_tweet,true]
      end
    rescue Exception => e
      [e,false]
    end  
  end

  def get_reply_tweet_from_gnip(count=0)
    
      gnip_tweet = GnipLib::GnipTweet.new(self.tweet_id.to_s)
      gnip_tweet.get_tweets

     if gnip_tweet.tweets[0] && gnip_tweet.tweets[0]["content"]
        self.update_attributes(:tweet_data=>gnip_tweet.tweets[0]["content"])
      elsif count < 7
        sleep(10)
        get_reply_tweet_from_gnip(count+1)
      else  
        ReplyTweetWorker.perform_async(self.id.to_s) 
      end 
  end

  def self.fetch_multi_conversation_for_parent(tweet_ids =[])
    if tweet_ids.present?
     
      tweets = Tweet.where(:id.in=>tweet_ids).parent_tweets.where(:tweet_status=>NO_SEARCH)
      parent_tweets = tweets.select {|tweet| tweet.tweet_data["inReplyTo"].blank?}[0...30]
    else  
      tweets = Tweet.parent_tweets.where(:tweet_status=>NO_SEARCH)
      parent_tweets = tweets.select {|tweet| tweet.tweet_data["inReplyTo"].blank?}[0...30]
    end
    if parent_tweets.present?
      begin

        parent_tweets.map {|tweet|tweet.update_attributes(:tweet_status=>CONVERSATION_PROCESS) }
        conversations = GnipLib::MultipleConversation.new(parent_tweets.collect{|tweet|tweet.tweet_id.to_s})
        conversations.get_conversation_for_parent_tweets
        set_parent_tweets_conversations(conversations,parent_tweets)
      rescue

        parent_tweets.map {|tweet|tweet.update_attributes(:tweet_status=>CONVERSATION_FAIL) }
        []
      end
    else
      []
    end  
  end

  def self.set_parent_tweets_conversations(conversations,tweets)
    tweet_ids = tweets.collect{|tweet|tweet.tweet_id.to_s}
    response = Hashie::Mash.new(conversations.response.parse_response)
    # Get Parent tweet hash
    parent_tweet_ids = []

    if response.results
      tweets.each do |tweet|
        begin
          # Select from Multiple tweet has child of tweet_id
          children_tweets = response.results.select {|result_tweet|result_tweet.inReplyTo.link.to_s.split("/").last == tweet.tweet_id }
          # Children Tweet is not found no tweet
          if children_tweets.present?      
            children_tweets.each do |child_tweet|
              
              child_tweet_id = child_tweet["object"]["id"].split(":").last
              # get tweet from db or create tweet and update parent id as current tweet
              children_tweet = Tweet.where(:tweet_id=>child_tweet_id).first
              if children_tweet.blank?
                children_tweet = Tweet.create(:tweet_id=>child_tweet_id,:tweet_data=>child_tweet,:parent_id=>tweet.id) 
              else
                children_tweet.update_attributes(:parent_id=>tweet.id)  
              end
            end
            tweet.update_attributes(:tweet_status=>HAS_CHILDREN)  
            parent_tweet_ids << tweet.id
          else
            tweet.update_attributes(:tweet_status=>NO_CHILDREN)
          end
        rescue Exception=> e
           tweet.update_attributes(:tweet_status=>RESPONSE_PARSE_ERROR)
        end  
      end
    end  
    parent_tweet_ids
  
  end
  def self.fetch_multi_conversation_for_in_reply_to(tweet_ids=[])
    if tweet_ids.present?
      tweets = Tweet.where(:id.in=>tweet_ids).parent_tweets.where(:tweet_status=>NO_SEARCH)
     
      child_tweets = tweets.select {|tweet| tweet.tweet_data["inReplyTo"]}[0...30]
    else
      tweets = Tweet.parent_tweets.where(:tweet_status=>NO_SEARCH)
      child_tweets = tweets.select {|tweet| tweet.tweet_data["inReplyTo"]}[0...30]
    end  
    if child_tweets.present?
      begin

        child_tweets.map {|tweet|tweet.update_attributes(:tweet_status=>CONVERSATION_PROCESS) }
        conversations = GnipLib::MultipleConversation.new(child_tweets.collect{|tweet|tweet.tweet_id.to_s})
        conversations.get_conversation_for_reply_tweets
        set_reply_tweets_conversatons(conversations,child_tweets)
      rescue
        child_tweets.map {|tweet|tweet.update_attributes(:tweet_status=>CONVERSATION_FAIL) }
        return []
      end
    else
      []  
    end  
  end


  def self.set_reply_tweets_conversatons(conversations,tweets)
    tweet_ids = tweets.collect{|tweet|tweet.tweet_id.to_s}
    response = Hashie::Mash.new(conversations.response.parse_response)
    parent_tweet_ids = []
    if response.results
      # Get Parent tweet hash
      tree_tweets = conversations.gnip_tweet.build_tree_tweets_hash
      # {:child_tweet_id=>parent_tweet_id}
      parent_tweet_hash = conversations.gnip_tweet.parent_tweet_ids
      
      tweets.each do |tweet|
        begin
          parent_tweet_id  = parent_tweet_hash[tweet.tweet_id]

          # IF Top most parent is out side one month range of gnip  then set no children even inReplyTO is present
          if tree_tweets[parent_tweet_id] && tree_tweets[parent_tweet_id]["available"]
            parent_tweet = Tweet.where(:tweet_id=>parent_tweet_id).first
            parent_tweet =  Tweet.create(:tweet_id=>parent_tweet_id,:tweet_data=>tree_tweets[parent_tweet_id]["content"]) if parent_tweet.blank?

            # Select from Multiple tweet has child of parent_tweet_id
            children_tweets = response.results.select {|result_tweet|result_tweet.inReplyTo.link.to_s.split("/").last ==  parent_tweet_id }
            # Children Tweet is not found no tweet
            if children_tweets.present?      
              children_tweets.each do |child_tweet|
                child_tweet_id = child_tweet["object"]["id"].split(":").last
                # get tweet from db or create tweet and update parent id as current tweet
                children_tweet = Tweet.where(:tweet_id=>child_tweet_id).first
                if children_tweet.blank?
                  children_tweet = Tweet.create(:tweet_id=>child_tweet_id,:tweet_data=>child_tweet,:parent_id=>parent_tweet.id) 
                else
                  children_tweet.update_attributes(:parent_id=>parent_tweet.id)  
                end
              end
              parent_tweet.update_attributes(:tweet_status=>HAS_CHILDREN)  
              tweet.update_attributes(:parent_id=>parent_tweet.try(:id))
              parent_tweet.search_keywords  = [parent_tweet.search_keywords + tweet.search_keywords].flatten.compact.uniq
              parent_tweet.save
              parent_tweet_ids << parent_tweet.id
            else
              parent_tweet.update_attributes(:tweet_status=>NO_CHILDREN)
              tweet.update_attributes(:tweet_status=>NO_CHILDREN)
            end
          else
            # If parent tweet is  out of range in one month
            tweet.update_attributes(:tweet_status=>NO_CHILDREN)
          end  
        rescue Exception=> e
           tweet.update_attributes(:tweet_status=>RESPONSE_PARSE_ERROR)
        end  
      end
    end  
    parent_tweet_ids
  
  end
 private
  def get_rule_connection
    GnipApi::Rule.new("https://api.gnip.com/accounts/IdyllicSoftware/publishers/twitter/streams/track/prod/rules.json")
  end

end

