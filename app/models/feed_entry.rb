class FeedEntry
  include Mongoid::Document
  include Mongoid::Timestamps
  include TfIdf

  paginates_per 100

  field :title
  field :url
  field :bitly_url
  field :author
  field :summary
  field :content
  field :important, type: Boolean
  field :published, type: Time
  field :categories, type: Array
  field :keywords, type: Array
  field :load_more_count,type: Integer,:default=>0
  field :load_more,type: Boolean,:default=>false
  field :is_searching,type: Boolean,:default=>false
  field :last_seen_at,type: DateTime
  has_and_belongs_to_many :search_keywords

  belongs_to :feed
  belongs_to :user

  validates_presence_of :user_id
  validates_presence_of :feed_id
  has_many :user_conversations,:dependent=>:destroy
  after_create :add_bitly_url

  def add_bitly_url
    self.bitly_url = BitlyUrl.new(self.url.to_s).short_url rescue nil  
    self.save
  end

  def generate_keywords(feed)
    tf_idf = TfIdf::TfIdfScore.new(self.content, feed.feed_entries.map{|fe| fe.content})
    self.keywords = tf_idf.get_score.sort_by{|k, v| v}.first(5).map{|a| a[0]}
    self.save
  end

  def search_by_keywords(keywords)
    search_keyword_tweets= []
    (keywords).split(",").flatten.compact.uniq.each do |keyword|
      if keyword.present?
        search_keyword =SearchKeyword.where(:keyword=>keyword.downcase).first
        search_keyword =SearchKeyword.create(:keyword=>keyword.downcase) if search_keyword.blank?
        self.search_keywords = [self.search_keywords,search_keyword].flatten.compact.uniq
        # start_searching
        self.save 
        #  Return keyword search time
        
        search_keyword_tweets.push << TweetSearchWorker.perform_async(search_keyword.id.to_s,self.id.to_s)
      end  
    end
    # search_keyword_tweets.flatten.compact.uniq
  end

  def remove_search_keyword(keyword)
    search_keyword_ids.delete(keyword.id)
    self.save
  end

  def tweets
    search_keywords.collect{|x|x.tweets.parent_tweets.has_children}
  end

  def relavant_tweets

    search_keywords.collect { |keyword| keyword.tweets.parent_tweets.has_children.relavant_tweets.includes(:replies).to_a }.flatten.compact.uniq
    
    # search_keywords.collect { |keyword| keyword.tweets.parent_tweets.relavant_tweets.to_a }.flatten.compact.uniq
    
  end

  def add_search_keyword(keyword)
    search_keyword = SearchKeyword.where(:keyword=>keyword.downcase).last
    search_keyword = SearchKeyword.create(:keyword=>keyword.downcase) if search_keyword.blank?
    self.search_keywords = [self.search_keywords,search_keyword].flatten.compact.uniq
    # start_searching
    self.save
    TweetSearchWorker.perform_async(search_keyword.id.to_s,self.id.to_s)
    search_keyword
  end

  def people_talking_count
    all_replies = self.tweets.flatten.collect { |t| t.replies.count }
    all_replies.present? ? all_replies.inject { |people_talking, reply_count| people_talking + reply_count } : 1
  end

  def last_reply_time
    all_replies = self.tweets.flatten.collect { |t| t.replies}
    all_replies.present? ?  all_replies.last.last.tweet_data['tweet']['created_at'] : nil
  end

  def clicks_count
    client = Bitly.client
    client.clicks(self.bitly_url).user_clicks
  rescue
    0
  end

  def start_load_more
    self.load_more = true
    self.load_more_count = 0
    self.save
  end

  def update_load_more_count
    self.load_more_count = self.load_more_count + 1
    if self.load_more_count == self.search_keywords.size
      self.load_more = false
      self.load_more_count = 0
      self.save
    end
  end
  
  def start_searching
    self.is_searching = true
    self.save
  end

  def update_searching
    self.is_searching = false
    self.save
  end

  def update_last_seen
    self.last_seen_at = DateTime.now
    self.save
  end

  def unseen_tweets
    search_keywords.collect { |keyword| keyword.tweets.parent_tweets.has_children.relavant_tweets.unseen_tweets(self.last_seen_at)}.flatten.compact.uniq
  end

end
