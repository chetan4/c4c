class UserConversation
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :user
  belongs_to :feed_entry
  # conversation starter tweet
  belongs_to :tweet
  # conversation reply tweets
  has_many :user_reply_tweets,:dependent=>:destroy

  validates_presence_of :feed_entry_id
  after_create :add_gnip_rules
  before_destroy :remove_rules

  def add_gnip_rules
    tweet_ids= get_conversation_tweet_ids
    if tweet_ids.present?
      rule  = get_rule_connection
      build_rules(rule,tweet_ids)
      rule.add_rules
    end  
    
  end

  def remove_rules
    # Same tweet can be follow my many users if it's follow by one user then remove
    if self.tweet.user_conversations.size < 2
      tweet_ids = get_conversation_tweet_ids
      if tweet_ids.present?
        rule  = get_rule_connection
        build_rules(rule,tweet_ids)
        rule.delete_rules
      end  
    end  

  end

  def get_conversation_tweet_ids
    [self.tweet.try(:tweet_id),self.tweet.get_all_replies.collect(&:tweet_id)].flatten.compact.uniq rescue  []
  end

  def build_rules(rule,tweet_ids=[])
    if tweet_ids.present?
      tweet_ids.each do |tweet_id|
        rule.build_rule("in_reply_to_status_id:#{tweet_id}","#{tweet_id}")
      end  
    end
  end

  def self.get_conversations(feed_entry, options = {})
    conversations = []
    twitter = TwitterLib.new(['rails'])
    tweets = twitter.get_tweets
    TweetSaveWorker.perform_async(tweets)
    reply_tweets = tweets.select{|t| t.tweet.in_reply_to_status_id}
    reply_tweets = reply_tweets.first(options[:limit]) if options[:limit]
    reply_tweets.each do |rt|
      conversations << twitter.get_conversation(rt.tweet).try(:reverse)
    end
    conversations
  end

  def self.create_conversation(conversation = [], feed_entry)
    conversation.reverse!
    conversation_hash = {
      feed_entry_id: feed_entry.id.to_s,
      user_id: feed_entry.user_id.to_s,
      parent_tweet_id: conversation.shift.id.to_s,
      tweets_in_reply: conversation.map{|t| t.id}
    }
    UserConversation.create(conversation_hash)
  end

  def add_reply(tweet_reply_id,replied_tweet_id)
    reply_tweet = self.user_reply_tweets.new(:tweet_id=>tweet_reply_id,:reply_tweet_id=>replied_tweet_id)
    reply_tweet.save
  end

  private

  def get_rule_connection
    GnipApi::Rule.new("https://api.gnip.com/accounts/IdyllicSoftware/publishers/twitter/streams/track/prod/rules.json")
  end
end
