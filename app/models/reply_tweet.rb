class UserReplyTweet
  include Mongoid::Document
  include Mongoid::Timestamps
  field :text
  belongs_to :user_conversation
  belongs_to :replied_tweet
  belongs_to :tweet
end
