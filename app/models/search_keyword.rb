class SearchKeyword
  include Mongoid::Document
  include Mongoid::Timestamps
  field :keyword, type: String
  field :last_searched_at, type:DateTime
  # validates_presence_of :keyword
  has_and_belongs_to_many :tweets,:index=>true
  has_and_belongs_to_many :feed_entries,:index=>true
  has_many :search_times,:dependent=>:destroy
  # SEARCH_HOURS = [1,2,4,8,16,32,64,128,256,512,720]
  SEARCH_HOURS = [128,256,512,720,1000]
  SEARCH_DAYS = (1...30).to_a

  # SEARCH_HOURS = [1,2,8,16,32,64]
  before_save :clean_keyword

  def clean_keyword
    self.keyword = self.keyword.strip
  end

  def search_tweet
    search_tweet_by_time
  end

  # Search by new time no offset logic
  def search_tweet_by_time(search_tweets = [])
    search_time = get_schedule
    if search_time
      self.fetch_tweets(search_time)
    else
      []
    end
  end

  def fetch_tweets(search_time)
    search = GnipLib::Search.new(self.keyword, :max_results=>500,
                                 :from_date=>search_time.mintime,
                                 :to_date=>search_time.maxtime)
    search.search_tweets
    search_time.search_count = search_time.search_count + 1
    search_time.save

    response = search.response.parse_response
    set_response(response, search_time)
  end

  # It will create tweets
  def set_response(response,search_time)
    tweets = []
    if response["results"].present?
      response["results"].each do |result|
        tweet_id = result["object"]["id"].split(":").last
        tweet = Tweet.where(:tweet_id=>tweet_id)

        if tweet.blank?
          tweet = self.tweets.create!(:tweet_id=>tweet_id,:tweet_data=>result)
        end
        tweets << tweet
      end
      search_time.tweets << tweets
      search_time.save!
      self.save!
    else
      []
    end
  end

  def set_search_time
    search = GnipLib::Search.new(self.keyword)
    search.count_tweets
    response = search.response.parse_response
    if response["results"].present?
      response["results"].each do |result|
        mintime = DateTime.parse(result["timePeriod"])
        maxtime = DateTime.parse(result["timePeriod"])+1.day
        search_time = self.search_times.where(:maxtime=>maxtime).first
        if search_time.present?
          search_time.update_attributes(:no_of_tweets=>result["count"])
        else
          self.search_times.create(:mintime=>mintime,
                                   :maxtime=>maxtime,
                                   :no_of_tweets=>result["count"])
        end
      end
    end
  end

  def get_schedule
    today_tweets = self.search_times.between_time(Date.today)
    set_search_time   if today_tweets.blank?
    set_current_schedule
  end

  # Find Out search time
  def set_current_schedule(search_days=30)
    search_order = self.search_times.order_by("maxtime desc")
      .where(:search_count.lt=>1).where(:mintime.gt=> Date.today - search_days.days)
    search_order.select {|search_time| search_time.tweets.blank?}
      .first || nil rescue  nil
  end


  def remove_feed_entry(feed_entry)
    self.feed_entry_ids.delete(feed_entry.id)
    self.save
  end
end
