class Keyword
  include Mongoid::Document

  field :keyword, type: String
  has_and_belongs_to_many :tweets
end
