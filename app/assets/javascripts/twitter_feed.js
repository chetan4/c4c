var C4C = C4C || {};
C4C.conversations= {}
C4C.feedUrl= ""
C4C.feedEntryId= ""
// For showing spinner o load more tweet
C4C.loadMoreResultCount = 0
C4C = (function($, window, document, Content4){
  Content4.twitterFeed = {
    // Render Individual Tweet Template 
    // view contain one tweet conversation json object
    showTweet : function(view,container){
      var template= C4C.templates['feedTweetTemplate'] || C4C.c4cTemplate.initFeedTweetTemplate()
      var html = template(view);
      // console.log(view)
      var container = container || "#left-section"
      
      
      $(container).append(html)
    },
    // It's display ideally page tweets
    // When user comes to any of article page and already tweets is present then 
    // It will fetch all tweets for each article
    //  Ideally called feom show page of feed entry
    showFeedTweets : function(url){
        ajaxOptions = {
          url: url,
          type:'GET',
          dataType:'json'
        };
        C4C.twitterFeed.getFeedTweet(ajaxOptions)
    },
    // When we click on load more tweets
    // Whatever key word is present for feed on page
    // Fire individual query to get further tweets for each keyword

    loadMoreTweets : function(){
      $("#load_more_tweets").on("click",function(e){
        
        // Content4.twitterFeed.showFeedTweets($(this).attr("href"))
        $(".tags ul li a").each(function(i,v){
          // Content4.twitterFeed.loadMoreTweetsForKeyword($(v).data("keyword-id"),C4C.feedEntryId)
          Content4.twitterFeed.loadMoreFeedEntryTweets()
          // Used for spinner on compete check counter value if it's 0 then hide spinner and show link
          C4C.loadMoreResultCount = i+1;
          // $(".load-more").addClass("loading");
          // $("#load_more_tweets").hide();

          // $(".load-more").addClass("loading");
           $(".load-more").text("Alright! We're off to search for more conversations. It's hard work and will take some time. We'll notify you once we are done")
          // $("#load_more_search_message").show();
          $("#load_more_tweets").hide();
        })

        return false; 
      });
    },
    loadMoreFeedEntryTweets : function(){
      var url ="/feed_entries/"+C4C.feedEntryId+"/load_more_tweets"
      ajaxOptions = {
          url: url,
          type:'GET',
          dataType:'json'

        };
        $.ajax(ajaxOptions).success(function(response){
          if(response.success){
         
          }
        });
    },
    // Get Further Tweets for each keyword
    loadMoreTweetsForKeyword : function(keyword_id,feed_entry_id){
      var url ="/feed_entries/"+feed_entry_id+"/load_more_tweets_for_keyword"
       ajaxOptions = {
          url: url,
          type:'GET',
          data: {keyword_id: keyword_id},
          dataType:'json'

        };
        $.ajax(ajaxOptions).success(function(response){
          if(response.success){
         
          }
        });
       // C4C.twitterFeed.getFeedTweet(ajaxOptions)
    },
    // When User is searching for keyword
    // Each keyword is split by comma and individual search will be in progres
    // 
    searchTweet : function(){
      $("#keyword_search_form").on("submit",function(event){
        
        var self = $("#keyword_search_form");

        var splitKeyword  = $("#keywords").val().split(",")
        var searchTagHtmls = []
        var searchKeywordTag= C4C.templates['searchKeywordTemplate'] || C4C.c4cTemplate.initSearchKeywordTagTemplate()
        if(splitKeyword[0]!="")
        {
          splitKeyword.each(function(v){
            keyword = $.trim(v.toLowerCase())
            var searchTagHtml = searchKeywordTag({keyword:keyword})
            searchTagHtmls.push(searchTagHtml)
          });
        }    
        ajaxOptions = {
          url: self.attr("action"),
          type:'GET',
          data: {keywords:$("#keywords").val()},
          dataType:'json',
          // Add to keyword to  search tags
          beforeSend: function( xhr ) {
            $("#keywords").val('');
            // searchTagHtmls.each(function(v)){
              $(".tags ul").append(searchTagHtmls)
            // })
          
          }
        };
        $.ajax(ajaxOptions).success(function(response){
          if(response.search_tweets){
            response.search_tweets.each(function(search_tweet,i){
              search_tweet.tweet.feed_entry_id = response.feed_entry_id.$oid
          
              C4C.twitterFeed.showTweet(search_tweet.tweet,"#content-area");
            });
            C4C.twitterFeed.updateTagAndSpinner(response)
            if(C4C.conversations.highlight)
              C4C.twitterFeed.highlightConversation()
          }
          // C4C.twitterFeed.updateTagAndSpinner(response)
          //   if(C4C.conversations.highlight)
          //     C4C.twitterFeed.highlightConversation()
        });
          // });  
        // }

      return false;  
      });
      
    }, 
    //  Fire Ajax query based on ajax options get tweets
    // Tweets might be search tweet,load more tweets,feed entry tweets
    // after fetching tweet json place into container
    getFeedTweet : function(ajaxOptions){
      $.ajax(ajaxOptions).success(function(response){
          
          response.search_tweets.each(function(search_tweet,i){
              search_tweet.tweet.feed_entry_id = response.feed_entry_id.$oid
              
              C4C.twitterFeed.showTweet(search_tweet.tweet,"#content-area");

            // });
            
          });
          C4C.twitterFeed.updateTagAndSpinner(response)
          if(C4C.conversations.highlight)
            C4C.twitterFeed.highlightConversation()
          // console.log(response)
        }); 
    },
    updateTagAndSpinner : function(response){
      if(response.search_keyword == null){
        C4C.twitterFeed.updateAllSearchKeywordCount(response);
        C4C.twitterFeed.updateSearchKeywordTag(response);
       }
      else if(response.search_keyword._id){
        C4C.twitterFeed.updateSearchKeywordTag(response);
        C4C.twitterFeed.countKeywordTweet(response.search_keyword.keyword);
      }
      else{

        C4C.twitterFeed.countKeywordTweet(response.search_keyword.keyword);
        
      } 
        C4C.twitterFeed.bindEvents();
        C4C.twitterFeed.hideLoadMoreSpinner();
    },
    // after fetching tweets set event on tweets
    // 
    bindEvents : function(){
      //  Used for removing vertical hight
      C4C.twitterFeed.setMasonry()
      C4C.twitterFeed.setFreetile();
      //  Binding Mark non relavant
      C4C.tweet.bindMarkNonRelavant();
      //  Binding Follow un flollw event
      C4C.tweet.bindFollowTweet();
      C4C.tweet.bindUnFollowTweet(false);
      // Bind Reply Tweet
      C4C.tweet.bindReplyTweetText();
    },

    
    // Get Feed Entry User COnversation IDS
    // Identify conversation Tweets on feed entry page
    // so we can show follow and unfollow
    // highlight is true then add colour to conversation
    setConversationTweets : function(url,highlight){
      
      ajaxOptions = {
          url: url,
          type:'GET',
          dataType:'json'
        };
        $.ajax(ajaxOptions).done(function(response){
          C4C.conversations = {tweets:response.tweets,tweetIds:response.tweet_ids,highlight:highlight}
          if(highlight)
            C4C.twitterFeed.highlightConversation()
       });
      
    },
    highlightConversation : function(){
      if(C4C.conversations.tweetIds){
        C4C.conversations.tweetIds.each(function(t){
          $("#tweet_"+t.$oid).css("background-color","#FFFFCC")
        })
      }  
    },
    setMasonry : function(){
      $('#content-area').masonry({
        itemSelector: '.post-area'
      });

    },
    setFreetile : function(){
      $('#content-area').freetile({animate:false});
    },
    hideLoadMoreSpinner : function(){
      // C4C.loadMoreResultCount = C4C.loadMoreResultCount - 1;
      // if(C4C.loadMoreResultCount<=0){
      //   $(".load-more").removeClass("loading");
      //   $("#load_more_tweets").show();
      //   C4C.loadMoreResultCount=0;
      // }
      $(".load-more").removeClass("loading");
        $("#load_more_tweets").show();
    },
    // When we remove search key word from remove from feed entry
    // remove associate tweets
    removeSearchKeyword : function(){
      $(".remove_search_keyword").on("click",function(e){
        var self = $(this);
      keywordId = $(this).data("keyword-id")
        ajaxOptions = {
          url: $(this).attr('href'),
          type:'GET',
          dataType:'json'
        };
        $.ajax(ajaxOptions).done(function(response){
          if(response.success==true)
          {
            self.parent('li').remove()
            $("[data-keyword-id="+keywordId+"]").remove();
            C4C.twitterFeed.setFreetile();
          }
         });
        return false; 
      });

    },
    // Feed Url Is used to get pasting url
    setFeedUrl : function(url){
      C4C.feedUrl= url
    },
    //  Setting feed entry id
    setFeedEntryID : function(id){
      C4C.feedEntryId = id
    },
    // When any tweet comes need to update search keyword count
    updateSearchKeywordTag : function(response){
      if(response.search_keyword){
        var view = {search_keyword_id:response.search_keyword._id.$oid,feed_entry_id:response.feed_entry_id.$oid,keyword:response.search_keyword.keyword}
         var searchKeywordTag= C4C.templates['searchKeywordTemplate'] || C4C.c4cTemplate.initSearchKeywordTagTemplate()
        var responseSearchTagHtml = searchKeywordTag(view)
        $("a[data-search-name='"+response.search_keyword.keyword+"']").parent().remove();
        // IF key word is already present thn just update coutn
        if($(".tags ul li[data-keyword-id='"+view.search_keyword_id+"']")[0]!=undefined){
          // C4C.twitterFeed.countKeywordTweet(view.keyword)
        }
        // If new key word is added then add to list
        else{
          $(".tags ul").append(responseSearchTagHtml);
          // C4C.twitterFeed.countKeywordTweet(view.keyword);
          C4C.twitterFeed.removeSearchKeyword();
        }  
        
        
      }
    },
   
    // Count one search key word tag
    countKeywordTweet : function(name)
    {
     var keyword= $("a[data-search-name='"+name+"']")
      
      var tweetLength = $(".post-tags span[data-search-name='"+name+"']").length

       keyword.parent().find(".searchTag").html(name+ " ("+ tweetLength +")")
        keyword.html("<i class='fontawesome-remove'></i>");
    },

    updateAllSearchKeywordCount : function(){
      $(".tags ul li a").each(function(i,v){
        C4C.twitterFeed.countKeywordTweet($(v).data("search-name"))
      });
    },

    toggleImportantFlag: function(){
      $('.imp_flag').on('click', function(event){
        var self = $(this);
        var current_flag = self.data('flag')
        ajaxOptions = {
          url: self.attr("href"),
          type:'get',
          dataType:'json'
        };
        $.ajax(ajaxOptions).done(function(response){
            if(response.success==true)
            {
              if (current_flag == 0){
                self.html('Make it unimportant');
                self.data('flag', 1);
              }
              else{
                self.html('Mark as important');
                self.data('flag', 0);
              }
            }
        });
        event.preventDefault();
      });
    },
    subscribePrivatePub : function()   {
      PrivatePub.subscribe("/tweets/"+C4C.feedEntryId, function(response, channel) {
        

        C4C.twitterFeed.renderTweets(response);
      });
    },
    renderTweets : function(response)
    {
      ajaxOptions ={
        url: "/feed_entries/"+C4C.feedEntryId+"/render_tweets",
        type:'get',
        data : {tweet_ids:response.tweet_ids,search_keyword_id:response.search_keyword_id},
        dataType:'json'
      }
      $.ajax(ajaxOptions).success(function(response){
          
          response.search_tweets.each(function(search_tweet,i){
            search_tweet.tweet.feed_entry_id = response.feed_entry_id.$oid
              C4C.twitterFeed.showTweet(search_tweet.tweet,"#content-area");
          });

          // console.log(response)
          C4C.twitterFeed.updateTagAndSpinner(response)
          if(C4C.conversations.highlight)
            C4C.twitterFeed.highlightConversation()
        });        
    },
    renderExistingTweets : function(tweets){
      var inGropus = tweets.inGroupsOf(5,"")
      var totalAjaxRequest = inGropus.length 
      var completeAjaxRequest = 0
      inGropus.each(function(tweetIds){
        $.ajax({
          url: "/feed_entries/"+C4C.feedEntryId+"/render_fetch_tweet",
          type:'get',
          data : {tweet_ids:tweetIds},
          dataType:'json',
          
          success :function(response){
            response.search_tweets.each(function(search_tweet,i){
            search_tweet.tweet.feed_entry_id = response.feed_entry_id.$oid
              C4C.twitterFeed.showTweet(search_tweet.tweet,"#content-area");
            });
          // C4C.twitterFeed.updateTagAndSpinner(response)  
           // C4C.twitterFeed.setFreetile();
          // if(C4C.conversations.highlight){
          //   C4C.twitterFeed.highlightConversation()
          // }  
         },
         complete :function(){
          
          completeAjaxRequest = completeAjaxRequest+1;
          if(completeAjaxRequest== totalAjaxRequest){
            C4C.twitterFeed.setFreetileAfterRender();
            C4C.twitterFeed.updateLastSeen()
          }  
         } 

        });


      });
    },
    // This method will render after all ajax request finish for first time
    setFreetileAfterRender : function(){
      C4C.twitterFeed.updateTagAndSpinner({})  
      C4C.twitterFeed.setFreetile();
      if(C4C.conversations.highlight){
        C4C.twitterFeed.highlightConversation()
      }  
      
    },
    updateLastSeen : function(){
      $.ajax({
        url: "/feed_entries/"+C4C.feedEntryId+"/update_last_seen",
        type:'get',
        dataType:'json',
         success :function(response){

         }
      });
    }
     
  }; 
  return Content4;


})(jQuery, this, this.document, C4C);
