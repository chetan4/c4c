var C4C = C4C || {};
C4C = (function ($, window, document, Content4) {
  Content4.feedEntries = {

    removeSearchKeyword: function (div) {
      $(div).find(".remove_search_keyword").on("click", function (e) {
        var self = $(this),
          ajaxOptions = {
            url: $(this).attr('href'),
            type: 'GET',
            dataType: 'json'
          };
        $.ajax(ajaxOptions).done(function (response) {
          if (response.success == true) {
            self.parent('li').remove();
            if ($(div).find('ul.keyword_ulist').children('li').length == 0) {
              $(div).find('.meta-count').remove();
              var msg = C4C.globals.getStartedMsg;
              $(div).find('.post-keywords h5').slideUp();
              console.log(div);
              $(div).find('ul + .add_keyword_link').remove();
              if ($(div).find('.post-keywords em').size()) {
                $(div).find('.post-keywords em').slideDown();
              } else {
                console.log(msg);
                $(div).find('.post-keywords').append(msg);
                C4C.feedEntries.addKeywordLink('#' + $(div).attr('id'));
              }
            }
          }
        });
        return false;
      });

    },


    addSearchKeyword: function (div) {
      var self = $(div).find('form');
      var splitKeyword = self.find("input.keywords").val().split(",");

      if (splitKeyword[0] != "") {
        splitKeyword.each(function (v) {
          ajaxOptions = {
            url: self.attr("action"),
            type: 'GET',
            data: {
              keywords: v
            },
            dataType: 'script',
            success: function () {
              $(div).find('.post-keywords').slideDown();
              $(div).find('.post-keywords em').slideUp();
              if ($(div).find('.post-keywords h5').size() === 0) {
                $(div).find('.post-keywords').prepend(C4C.globals.HTMLKeywordsHdg);
              } else {
                $(div).find('.post-keywords h5').slideDown();
              }
              console.log('keyword count', C4C.globals.getKeywordCount(div));
              if (C4C.globals.getKeywordCount(div) === 1) {
                $(div).find('.keyword_ulist').after('<a href="#" class="add_keyword_link"><i class="fontawesome-minus"></i></a>');
                C4C.feedEntries.addKeywordLink('#' + $(div).attr('id'));
              }
            }
          };
          $.ajax(ajaxOptions);
        });
      }
    },

    loadMoreBlogPosts: function () {
      $('#load_more_link').on('click', function (e) {
        if ($(this).attr('href') != 'void(0);') {
          ajaxOptions = {
            url: $(this).attr('href'),
            type: 'GET',
            dataType: 'script'
          };
          $.ajax(ajaxOptions);
          e.preventDefault();
        }
        e.preventDefault();
      });
    },

    searchBlogPosts: function () {
      $('#search_blog_input').on('keypress', function (event) {
        if (event.keyCode === 13) {
          if ($(this).val() != '') {
            ajaxOptions = {
              url: $(this).data('url'),
              type: 'GET',
              dataType: 'script',
              data: {
                search: $(this).val()
              },
              beforeSend: function (xhr) {
                $('.loading').show();
              }
            };
            $.ajax(ajaxOptions);
          }
        }
      });
    },

    addKeywordLink: function (div) {
      $(div).find('.add_keyword_link').on('click', function (event) {
        event.preventDefault();
        $(div).find('.post-keywords em').slideDown().hide();
        var i = $(this).find('i');
        if ($(i).hasClass('fontawesome-plus')) {
          $(i).removeClass('fontawesome-plus').addClass('fontawesome-minus');
          $(div).find('.blog-list-addkeyword-form form').show();
          return;
        } else if ($(i).hasClass('fontawesome-minus')) {
          $(i).removeClass('fontawesome-minus').addClass('fontawesome-plus');
          $(div).find('.blog-list-addkeyword-form form').hide();
          return;
        }
        $(div).find('.blog-list-addkeyword-form form').show();
        if ($(div).find('keyword_ulist li').size() > 0) {
          $(div).find('em').hide();
        }
        //if ( $( this ).parent().parent().hasClass( 'post-keywords' )) {}
      });

      $(div).find('.hide_keyword_form').on('click', function (event) {
        //$( div ).find( '.add_keyword_link' ).slideDown();
        if (C4C.globals.getKeywordCount(div) === 0) {
          $(div).find('.post-keywords em').show();
        }
        $(div).find('.blog-list-addkeyword-form form').hide();
        if (C4C.globals.getKeywordCount(div) === 0) {
          $(div).find('em').show();
        }
        $(div).find('ul + a > i').removeClass('fontawesome-minus').addClass('fontawesome-plus');
      });
    },

    toggleImportantFlag: function (div) {
      var impFlagSpinnerID;
      $(div).find('.imp_flag').on('click', function (event) {
        impFlagSpinnerID = Date.now();
        // $(this).after('<img id="' + impFlagSpinnerID + '" src="/assets/loader.gif" alt="loading" style="margin-left: 10px;" />')
        var self = $(this);
        var current_flag = self.data('flag');
        ajaxOptions = {
          url: self.attr("href"),
          type: 'get',
          dataType: 'json'
        };
        $.ajax(ajaxOptions).done(function (response) {
          $('#' + impFlagSpinnerID).fadeOut(function () {
            $(this).remove();
          });
          if (response.success == true) {
            if (current_flag == 0) {
              self.html('<i class=\"fontawesome-star\"></i>');
              self.data('flag', 1);
            } else {
              self.html('<i class=\"fontawesome-star-empty\"></i>');
              self.data('flag', 0);
            }
          }
        });
        event.preventDefault();
      });
    }

  };
  return Content4;


})(jQuery, this, this.document, C4C);

C4C.globals = {
  getStartedMsg: '<em>I am not listening to anything right now. Add some keywords to get started. <a href="#" class="add_keyword_link">Get me started.</a></em>',

  HTMLAddKeywordLink: '<a class="add-more add_keyword_link">Add keywords <i class="fontawesome-chevron-down"></i></a>',

  HTMLKeywordsHdg: '<h5>Tracking Keywords</h5>',

  getKeywordCount: function (elt) {
    return $(elt).find('.keyword_ulist li').size();
  }
};
