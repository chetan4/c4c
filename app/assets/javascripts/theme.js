// Tooltip

jQuery( function( $ ) {
  $.fn.c4cTooltip = function( options ) {
    var showTooltip = function() {
      console.log( 'showing Tooltip' );
      $( this ).after( '<span class="c4c-tooltip-content">' + $( this ).data( 'c4ctooltip' ) + '</span>' );
    };
    var hideTooltip = function() {
    };
    var disableTooltip = function() {
    };

    this.mouseenter( function() {  // showing Tooltip
      $( this ).after( '<span class="c4c-tooltip-content">' + $( this ).data( 'c4ctooltip' ) + '</span>' );
    }).mouseleave( function() {  // hiding Tooltip
      $( '.c4c-tooltip-content' ).fadeOut( 200, function() { this.remove(); });
    });
    return this;
  }
}( jQuery ));
