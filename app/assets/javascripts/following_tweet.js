var C4C = C4C || {};
C4C = (function($, window, document, Content4){
   Content4.followingTweet = {
     showTweet : function(view,container){
      var template= C4C.templates['folowwingTweetTemplate'] || C4C.c4cTemplate.initFollowingTweetTemplate()
      var html = template(view);
      // console.log(view)
      var container = container || "#left-section"
      $(container).append(html)
    },
    showFollowingTweets : function(url){
        ajaxOptions = {
          url: url,
          type:'GET',
          dataType:'json'
        };
        C4C.followingTweet.getFollowingTweet(ajaxOptions)
    },
    getFollowingTweet : function(ajaxOptions){
      $.ajax(ajaxOptions).
        done(function(response){
          response.search_tweets.each(function(search_tweet,i){
            // i = i+1;
            // if(i%2==0)
            //   C4C.twitterFeed.showTweet(search_tweet.tweet,"#left-section")
            // else
            //   C4C.twitterFeed.showTweet(search_tweet.tweet,"#right-section")
            search_tweet.tweet.feed_entry_id = response.feed_entry_id.$oid
            
            C4C.followingTweet.showTweet(search_tweet.tweet,"#content-area");

          });
          C4C.followingTweet.bindEvents();
        }); 
    },
    bindEvents : function(){
      C4C.twitterFeed.setMasonry()
      // C4C.twitterFeed.setFreetile();
      C4C.tweet.bindReplyTweetText();
      C4C.tweet.bindUnFollowTweet(true);
    }

   };

  return Content4;


})(jQuery, this, this.document, C4C);
