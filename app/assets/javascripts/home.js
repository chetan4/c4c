$(document).ready(function() {
  $('.enter-blog-url').css('display', 'none');
});

$(document).scroll(function(){
  if( $(this).scrollTop() > 460 ){
    $('.enter-blog-url').css('margin-top', '-53px');
    $('.enter-blog-url').css('display', 'block');
    $('#logo-signin > .logo-container').addClass('logo-left').removeClass('logo-center');

  }
  else if ( $(this).scrollTop() < 449 ){
    $('.enter-blog-url').css('margin-top', '-135px');
    $('#logo-signin > .logo-container').removeClass('logo-left').addClass('logo-center');
  }
  if( $(this).scrollTop() > 485 ){
    $('#button-main-twitter-signup').css('margin-bottom','54px');
    // $('.registered-user').css('opacity','0');
    $('.registered-user').addClass('fixed-right');
  }
  else if ( $(this).scrollTop() < 449 ) {
    $('.registered-user').removeClass('fixed-right');
    $('#button-main-twitter-signup').css('margin-bottom','0');
  }

});