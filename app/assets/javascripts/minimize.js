$(document).ready(function($) {
$(".toggle-replies").click(function(e) {
  e.preventDefault();
  var parents = $(this).parents('.post-area-heading').siblings('.post-tweets');
  $(this).toggleClass('active');
  parents.slideToggle('fast');
  });
});