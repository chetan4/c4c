$(function() {
  $('.fontawesome-reply').click(function(e) {
    e.preventDefault();
    if ($(this).siblings('textarea').length < 1){ 
      var buttonArea = '<div class="button-area clearfix"><button class="cancel-reply">Cancel</button> <button class="reply-tweet">Tweet</button></div>';
      $('<textarea></textarea>', {
        // placeholder: " @reply to",
        // type: 'text',
        // class: 'reply-box'
      }).insertAfter(this).after(buttonArea),
      $('.cancel-reply').click(function() {
        $(this).parent('.button-area').hide().siblings('.reply-box').remove();
      })
    }
  });
});