$(function(){
//after clicking the 'mark as not relevant'
  $('.relevance').click(function(e){
    e.preventDefault();
    var undoText = '<div class="undo-conv"><span class="unrelevant">Unrelevant Conversation</span> <button class="undo">Undo</button>';
    // var confirmation = '<button class="delete">Delete</button><button class="cancel">Cancel</button>';
    $(this).parents('.post-area').children().not('.relevance').css('opacity', '.3');
    $(this).closest('.post-area-heading').before(undoText);
    // $('delete-conv > span').after(confirmation);
    $('button.undo').click(function(){
      $('.undo-conv').remove();
      $('.post-area').children().css('opacity','1');
    });
  })
});