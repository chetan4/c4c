// {{#compare var1 var2 operator="=="}}
Handlebars.registerHelper('compare', function(lvalue, rvalue, options) {

    if (arguments.length < 3)
        throw new Error("Handlerbars Helper 'compare' needs 2 parameters");

    operator = options.hash.operator || "==";

    var operators = {
        '==':       function(l,r) { return l == r; },
        '===':      function(l,r) { return l === r; },
        '!=':       function(l,r) { return l != r; },
        '<':        function(l,r) { return l < r; },
        '>':        function(l,r) { return l > r; },
        '<=':       function(l,r) { return l <= r; },
        '>=':       function(l,r) { return l >= r; },
        'typeof':   function(l,r) { return typeof l == r; }
    }

    if (!operators[operator])
        throw new Error("Handlerbars Helper 'compare' doesn't know the operator "+operator);

    var result = operators[operator](lvalue,rvalue);

    if( result ) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }

});
Handlebars.registerHelper("isUrls", function(entities,block) {

    if(entities && entities.urls.length >= 1)
    {
      return block.fn(this);
    }

});
Handlebars.registerHelper("isHashTag", function(entities,block) {

    if(entities && entities.hashtags.length >= 1)
    {   
      return block.fn(this);
    }

});

Handlebars.registerHelper("displayHashTag", function(entities,block) {
  if(entities)
    return entities.hashtags.map(function(i,v){return "#"+i.text }).join(",")
});

Handlebars.registerHelper("displayScreenUrl", function(screen_name) {
   return "https://twitter.com/"+screen_name
});
Handlebars.registerHelper("displayNoOfResponses", function(replies) {
   return replies.length
});
Handlebars.registerHelper("showTweetTime", function(datetime) {
   if(Date.create(datetime).minutesAgo() < 60)
    return (Date.create(datetime).minutesAgo()) +" minutes back"
   else if(Date.create(datetime).hoursAgo() < 24)
    return (Date.create(datetime).hoursAgo()) +" hours ago"
   else if(Date.create(datetime).daysAgo() < 24)
    return (Date.create(datetime).daysAgo()) +" days ago" 
});
// Display tweet or user location
Handlebars.registerHelper("displayTweetLocation", function(tweet) {
  if( tweet && tweet.actor.location && tweet.actor.location.displayName)
    return tweet.actor.location.displayName
  else
    // return tweet.place || tweet.user.timezone || tweet.user.location  || "Not Available"
    "Not Available"
});
Handlebars.registerHelper("isFollowConversation", function(tweet_id,block) {
    
    if(C4C.conversations.tweetIds && C4C.conversations.tweetIds.find(tweet_id)){
      return block.fn(this);
  }
    else{
      return block.inverse(this);
    }

});
Handlebars.registerHelper("screenName", function(twiter_link) {
  if(twiter_link)
    return twiter_link.split("/").last()
  else
    return ""

});
Handlebars.registerHelper("searchKeywordIds", function(searchKeywords) {
  
  return searchKeywords[0]._id.$oid
  
});
Handlebars.registerHelper("displaySearchKeywordTag", function(searchKeyword) {
  return searchKeyword + "("+ C4C.twitterFeed.countKeywordTweet(searchKeyword)+")"
  
  
});