var VerifyFeed = {
  listener: function(){
    self = this;
    var lastFeedURL = $( '#top-url-input' ).val();
    var handleFeed = function() {
      console.log('handling feed');
      self.inputUrl = ($('#url').val() || $('#top-url-input').val());
      if ( self.inputUrl ) { // Feed URL inpur is not empty.
        self.typingTimer = setTimeout(self.ajax_callback(self), self.doneTypingInterval);
      }
    };
    $( '#try-it-btn' ).click( handleFeed );
    $('#top-url-input').keypress(function(e){
     if (e.keyCode === 13) { 
      console.log('enter key pressed');
      e.preventDefault();
      $('#try-it-btn').click();
      };
    });
  },
  ajax_callback: function(self){
    url = self.path + self.inputUrl;
    self.top_signin.disabled = true;
    var loadingInterval;
    $.ajax({
       url: url,
       type:'Get',
       beforeSend : function(xhr){
        $(".indicator").show();

        $(".waiting-message").show();
        $(".success-message").hide()
        $(".failed-message").hide()
        var feedValidatingDivHt = 0;
        var feedValidatingDiv = $( '#feed-validating' ).show().css({'background-color': 'rgba(0,0,0,0.3)'});
        var feedValidatingDiv = $( '#feed-validating > div' );
        loadingInterval = setInterval( function() {
          if ( feedValidatingDivHt >= 40 ) {
            clearInterval( loadingInterval );
          }
          $( feedValidatingDiv ).show().css( 'height', feedValidatingDivHt++ );
        }, 30 );
       },
       success : function(data){
        clearInterval( loadingInterval );
        $( '#feed-validating' ).hide();
        if(data == true){  // Feed URL valid.
          //console.log('self.main_singnin.disabled = false;');
          $('.import-blog-form').css({'border-color': 'green'});
          $('#try-it-btn').hide();
          $('#button-top-twitter-signup').show().css({'top': '0'});
          $('.invalid-url').fadeOut(function() { $( '.invalid-url' ).remove();});
          self.top_signin.disabled = false;
          $(".success-message").show()
          $( '#step-indicator' ).text( 'Step 2 of 2' );
          $( '.import-blog-form' ).data( 'step', '2' );
        }
        else{
          $('.import-blog-form').css({'border-color': 'red'});
          if ( $('.invalid-url').size() === 0 ) {
            // Avoid repeating the error test on multiple focusout events.
            $( '.import-blog-form' ).after( '<span class="invalid-url" style="color: red;">Invalid URL. Please try again.</span>' );
          }
          self.top_signin.disabled = true;
          $(".failed-message").show();
          $( '#step-indicator' ).text( 'Step 1 of 2' );
          $( '.import-blog-form' ).data( 'step', '1' );
          $('#try-it-btn').show();
          $('#button-top-twitter-signup').hide();
       }
       $(".indicator").hide();
        $(".waiting-message").hide();
      }
    })
  },
  init: function(){
    this.typingTimer =  null
    this.doneTypingInterval = 2000;
    this.inputUrl = null;
    this.path = '/feeds/validate_feed/?url=';
    this.feed_url_input = $('.user-input-feed-url');
    this.main_singnin = document.getElementById("button-main-twitter-signup");
    this.top_signin = document.getElementById("button-top-twitter-signup");
    // this.main_singnin.disabled = true;
    // this.top_signin.disabled = true;
    this.listener();
  }
}

var C4C = C4C || {};
