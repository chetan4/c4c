//after clicking the 'mark as not relevant'
$('.post-area-heading > a:last-child').click(function(e){
  e.preventDefault();
  // var buttons = '<button class="delete">Delete</button> 
  // <button class="cencel"> Cancel </button>';
  var trashText = '<div class="delete-conv"><span class="delete">Unrelevant Conversation</span> <div class="arrow"></div> <button class="delete">Undo</button>';
  // var confirmation = '<button class="delete">Delete</button><button class="cancel">Cancel</button>';
  $(this).parents('.post-area').children().not('.delete-conv').css('opacity', '.3');
  $(this).closest('.post-heading').before(trashText);
  // $('delete-conv > span').after(confirmation);
  $('button.cancel').click(function(){
  $('.delete-conv').remove();
  $('.post-area').children().css('opacity','1');
  });
})