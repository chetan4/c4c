var C4C = C4C || {};
C4C.templates  = {}
C4C = (function($, window, document, Content4){
  Content4.c4cTemplate = {
    initFeedTweetTemplate : function(){
      var tweetTemplate = $("#tweet_template").html()
      Handlebars.registerPartial("tweet_reply", $("#reply_tweet_template").html());
      return C4C.templates['feedTweetTemplate']= Handlebars.compile(tweetTemplate);

    },
    initReplyTweetTemplate : function(){
      var tweetTemplate = $("#reply_tweet_template").html()
      return C4C.templates['replyTweetTemplate']= Handlebars.compile(tweetTemplate);
    },
    initMarkNonRelavantTemplate : function(){
      var tweetTemplate = $("#mark_non_relavant_template").html()
      return C4C.templates['markNonRelavantTemplate']= Handlebars.compile(tweetTemplate);
    },
    initReplyTextTemplate : function(){
      var tweetTemplate = $("#reply_text_template").html()
      return C4C.templates['replyTextTemplate']= Handlebars.compile(tweetTemplate);
    },
    initFollowingTweetTemplate : function(){
      var tweetTemplate = $("#following_tweet_template").html()
      Handlebars.registerPartial("tweet_reply", $("#reply_tweet_template").html());
      return C4C.templates['followingTweetTemplate']= Handlebars.compile(tweetTemplate);

    },
    initSearchKeywordTagTemplate : function(){
      var tweetTemplate = $("#search_keyword_tag_template").html()
      return C4C.templates['searchKeywordTemplate']= Handlebars.compile(tweetTemplate);

    },
  };

  
  return Content4;


})(jQuery, this, this.document, C4C);
