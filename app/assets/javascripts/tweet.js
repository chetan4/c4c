var C4C = C4C || {};

C4C = (function($, window, document, Content4){
  Content4.tweet = {
    markNonRelavant : function(tweet_id){
      ajaxOptions = {
        url: "/mark_non_relavant",
        data: {id:tweet_id},
        type:'Get',
        dataType:'json'
      };
      $.ajax(ajaxOptions).done(function(response){
        if(response.success){
          // When we remove tweet update search key word count
         searchKeywords= C4C.tweet.getTweetSearchKeyword(tweet_id)
           
          $("#tweet_"+tweet_id).remove();
          searchKeywords.each(function(v){
            Content4.twitterFeed.countKeywordTweet(v);

          })
          
          C4C.twitterFeed.setFreetile()
        }
        else{

        }
      });
    },

    bindMarkNonRelavant : function(){
      $(".relevance").unbind().on("click",function(e){
        C4C.twitterFeed.setFreetile();
        var tweetId = $(this).data("id");
        
        var template= C4C.templates['markNonRelavantTemplate'] || C4C.c4cTemplate.initMarkNonRelavantTemplate()
        view = {id:tweetId}
        html = template(view)
        if ( $(this).parents('.post-area-heading').siblings('.undo-conv').length < 1 ) {
          $(this).parents('.post-area').children().not('.relevance').css('opacity', '.3');
          $(this).closest('.post-area-heading').before(html);
          Content4.tweet.bindUndoMarkNonRelavant(tweetId);
          Content4.tweet.bindConfirmNonRelavant(tweetId);
        }
        // Content4.tweet.markNonRelavant($(this).data("id"))
        return false;
      });
        
    },
    bindUndoMarkNonRelavant : function(tweetId){

      $('button#undo_'+tweetId).click(function() {
          $('#non_relavant_'+tweetId).remove();
          $('.post-area').children().css('opacity','1');
        });
    },
    bindConfirmNonRelavant : function(tweetId)
    { 
       $('#unrelevant_'+tweetId).click(function(e){
        Content4.tweet.markNonRelavant(tweetId)
         // C4C.twitterFeed.setFreetile()
       });
    },

    replyTweetText : function(tweetId){
      $('#reply_tweet_'+tweetId).click(function() {
        var tweetText =$("#reply_box_"+tweetId).val()
        if(tweetText.length <140){
          var feedEntryId = C4C.feedEntryId;
          ajaxOptions = {
            url: "/tweet_it",
            data: {tweet_id:tweetId,feed_entry_id:feedEntryId,tweet_text:tweetText},
            type:'Get',
            dataType:'json'
          };
          $.ajax(ajaxOptions).done(function(response){
            if(response.success){

              var template= C4C.templates['feedTweetTemplate'] 
              view = {tweet: response.tweet}
              response.tweet.feed_entry_id = response.feed_entry_id
              var html = template(response.tweet);
              $("#tweet_"+response.parent_id.$oid).replaceWith(html)
              C4C.tweet.bindReplyTweetText();
              C4C.conversations.tweetIds.push(response.parent_id)
              if(C4C.conversations.highlight)
                C4C.twitterFeed.highlightConversation()
              C4C.twitterFeed.setFreetile()
            }
            else{
              if(response.parent_id){
                $("#reply_box_"+response.tweet_id).parent().hide()
                $("#tweet_message_"+response.tweet_id).html("Tweet has been queued")
                C4C.conversations.tweetIds.push(response.parent_id)
                if(C4C.conversations.highlight)
                  C4C.twitterFeed.highlightConversation()
              }
              else{
                 $("#reply_box_"+response.tweet_id).parent().hide()
                  $("#tweet_message_"+response.tweet_id).html(response.message)
              }
            }
          }); 
        }
        else{
          $("#reply_error_"+tweetId).text("Tweet should be less than 140 character")
        }   
      });
    },
    bindReplyTweetText : function(){
      $('.fontawesome-reply').on("click",function(e){
        e.preventDefault();
        var tweetId = $(this).data("id");
        var screenName = $(this).data("screen-name");
        var template= C4C.templates['replyTextTemplate'] || C4C.c4cTemplate.initReplyTextTemplate()
        view = {id:tweetId,screen_name:screenName}
        html = template(view)
        $(this).parent().find(".replyBox:first").html(html)
        
        Content4.tweet.bindCancelTweetText(tweetId);
        Content4.tweet.replyTweetText(tweetId);
        Content4.tweet.bindPasteUrl(tweetId);
        C4C.twitterFeed.setFreetile()
        // Content4.tweet.markNonRelavant($(this).data("id"))
      
      });
    },
    bindCancelTweetText : function(tweetId){
      $('.cancel-reply').click(function() {
        $(this).parent('.button-area').hide().siblings('.reply-box').hide();
        C4C.twitterFeed.setFreetile()
      });
    },
    bindPasteUrl : function(tweetId){
      $("#reply_box_"+tweetId).on("paste",function(e){
        var currentVal = $("#reply_box_"+tweetId).val()
        $("#reply_box_"+tweetId).val(currentVal+C4C.feedUrl)
        return false;
      });
    },

    followTweet : function(tweetId,feedEntryId){
      ajaxOptions = {
        url: "/follow_conversation",
        data: {tweet_id:tweetId,feed_entry_id:feedEntryId},
        type:'Get',
        dataType:'json'
      };
      $.ajax(ajaxOptions).done(function(response){
        if(response.success){
          $("#follow_"+tweetId).removeClass("fontawesome-follow")
          $("#follow_"+tweetId).addClass("fontawesome-unfollow")
          $("#follow_"+tweetId).text("Unfollow Conversation")
          Content4.tweet.rebindFollowTweet(tweetId)
          C4C.conversations.tweetIds.push({$oid: tweetId})
          if(C4C.conversations.highlight)
            C4C.twitterFeed.highlightConversation()
          // $("#tweet_"+tweet_id).remove();
          // C4C.twitterFeed.setFreetile()
        }
        else{

        }
      });
    },
    bindFollowTweet : function(){
      $('.fontawesome-follow').click(function(e) {
        // var followButton = $('fontawesome-unfollow');
        e.preventDefault();
        $(this).toggleClass("fontawesome-unfollow").toggleClass('fontawesome-follow');
        $('.fontawesome-unfollow').text('Unfollow Conversation');
        $('.fontawesome-follow').text('Follow Conversation');
        var tweetId = $(this).data("id")
        Content4.tweet.follow(tweetId)
        var tweetId = $(this).data("id")
        var feedEntryId = $(this).data("feed-entry-id")
        C4C.tweet.follow(tweetId,feedEntryId)

      });
    },

    unFollowTweet : function(tweetId,feedEntryId,removeFeed){
      ajaxOptions = {
        url: "/unfollow_conversation",
        data: {tweet_id:tweetId,feed_entry_id:feedEntryId},
        type:'Get',
        dataType:'json'
      };
      $.ajax(ajaxOptions).success(function(response){
        if(response.success){
          $("#follow_"+tweetId).removeClass("fontawesome-unfollow")
          $("#follow_"+tweetId).addClass("fontawesome-follow")
          $("#follow_"+tweetId).text("Follow Conversation")
          

          if(removeFeed && removeFeed== true){
            $("#tweet_"+tweetId).remove();
            C4C.twitterFeed.setFreetile()
          }
          else{
            Content4.tweet.rebindFollowTweet(tweetId)
          }
          if(C4C.conversations.highlight){
            $("#tweet_"+tweetId).css("background-color","")
          }
        }

        else{

        }
      });
    },

    bindUnFollowTweet : function(removeFeed){
       $('.fontawesome-unfollow').click(function(e) {
       // var followButton = $('fontawesome-unfollow');
        e.preventDefault();
        // $(this).toggleClass("fontawesome-unfollow")
        // $('.fontawesome-unfollow').text('Unfollow Conversation');
        var tweetId = $(this).data("id")
        Content4.tweet.unFollow(tweetId,removeFeed)        
      });  
    },
    follow : function(tweetId){
      var selector = $("#follow_"+tweetId)
      var feedEntryId = selector.data("feed-entry-id")
      C4C.tweet.followTweet(tweetId,feedEntryId)
    },

    unFollow : function(tweetId,removeFeed){
      var selector = $("#follow_"+tweetId)
      var feedEntryId = selector.data("feed-entry-id")
      C4C.tweet.unFollowTweet(tweetId,feedEntryId,removeFeed)
    },
    rebindFollowTweet : function(tweetId){
       
        $("#follow_"+tweetId).unbind().on("click",function(e){
          e.preventDefault();
          if($("#follow_"+tweetId).hasClass("fontawesome-unfollow")){
            Content4.tweet.unFollow(tweetId) 
          }
          else{
            Content4.tweet.follow(tweetId)  
          }         
        });
    },
    getTweetSearchKeyword : function(tweet_id){
      searchKeywords =[]
      $("#tweet_"+tweet_id+" .post-tags span").each(function(i,v){
            searchKeywords[i]= $(v).data("search-name");
      });
      return searchKeywords;
    }
  }; 

return Content4;


})(jQuery, this, this.document, C4C);
