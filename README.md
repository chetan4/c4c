c4c
===

Inhouse Product code named c4c

STAGING SERVER

    sudo /etc/init.d/redis_6379 start
    cap staging deploy
    cap staging rvm:hook sidekiq:stop
    cap staging rvm:hook sidekiq:start
    cap staging rvm:hook unicorn:stop
    cap staging rvm:hook unicorn:start
    cap staging rvm:hook private_pub:stop
    cap staging rvm:hook private_pub:start

    cap production deploy
    cap production rvm:hook sidekiq:stop
    cap production rvm:hook sidekiq:start
    cap production rvm:hook unicorn:stop
    cap production rvm:hook unicorn:start
    # Create Mongo Index
    cap production rvm:hook mongo_index:create
    # remove mongo index
    cap production rvm:hook mongo_index:remove
    
    cap production rvm:hook private_pub:stop
    cap production rvm:hook private_pub:start
    cap production rvm:hook gnip_stream:start
    
Local
    bundle exec rackup private_pub.ru -s thin -E production
    cd gnip_stream && ruby gnip_stream.rb 
    # or
    ruby gnip_stream/gnip_daemon.rb start
    
Monit script for sidekiq

check process sidekiq_c4c with pidfile /home/ubuntu/apps/c4c/current/tmp/pids/sidekiq.pid

  start program = "/bin/bash -c -l 'cd /home/ubuntu/apps/c4c/current && ( RAILS_ENV=production  /home/ubuntu/.rvm/bin/rvm 2.0.0-p247 do bundle exec sidekiq -d -i 0 -P   /home/ubuntu/apps/c4c/current/tmp/pids/sidekiq.pid -e production -C /home/ubuntu/apps/c4c/current/config/sidekiq.yml -L /home/ubuntu/apps/c4c/current/log/sidekiq.log)'" as uid ubuntu as gid ubuntu
stop program = "/bin/bash -c -l 'kill -s TERM `cat /home/ubuntu/apps/c4c/current/tmp/pids/sidekiq.pid`'" with timeout 90 seconds
  if totalmem is greater than 300 MB for 3 cycles then restart # eating up memory?
  group c4c_sidekiq


