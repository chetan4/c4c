class CreateFeeds < ActiveRecord::Migration
  def change
    create_table :feeds do |t|
      t.string :url
      t.belongs_to :user
      t.timestamps
    end
  end
end
