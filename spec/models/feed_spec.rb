require 'spec_helper'

describe Feed do
  FEED_URL = 'http://feeds2.feedburner.com/ZenAndTheArtOfRubyProgramming'
  before :each do
    @feed = Feed.new
  end

  it 'has an associated user' do
    @feed.update_attributes(url: FEED_URL, active: true)
    @feed.should have(1).errors_on(:user_id)
  end

  it 'should have a url' do
    @feed.update_attributes(user_id: 1, active: true)
    @feed.should have(1).errors_on(:url)
  end

  it 'should save if all attributes are present' do
    param = {
      user_id: 1,
      url: FEED_URL,
      active: true
    }
    @feed.update_attributes(param)
    @feed.should be_valid
  end
end
