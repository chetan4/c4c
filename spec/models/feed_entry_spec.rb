require 'spec_helper'

describe FeedEntry do
  before :each do
    @feed_entry = FeedEntry.new
  end

  it 'should have an associated user' do
    @feed_entry.update_attributes(feed_id: 1)
    @feed_entry.should have(1).errors_on(:user_id)
  end

  it 'should belong to a feed' do
    @feed_entry.update_attributes(user_id: 1)
    @feed_entry.should have(1).errors_on(:feed_id)
  end

  it 'should be a valid feed' do
    hash = {
      feed_id: 1,
      user_id: 1
    }
    @feed_entry.update_attributes(hash)
    @feed_entry.should be_valid
  end
end
