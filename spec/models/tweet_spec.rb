require 'spec_helper'

describe Tweet do
  before :each do
    @tweet = Tweet.new
  end

  it 'should have a user associated' do
    @tweet.update_attribute(:tweet_data, {test: 'test hash'})
    @tweet.should have(1).errors_on(:user_id)
  end
end
