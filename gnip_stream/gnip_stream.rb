require 'rubygems'
# require 'bundler/setup'
require 'typhoeus'
require 'pry'
require 'json'
require 'multi_json'
require 'oj'
require 'redis'
require 'sidekiq'
require 'securerandom'
require 'daemons'


class GnipStream

  def initialize
    @request = Typhoeus::Request.new("https://stream.gnip.com/accounts/IdyllicSoftware/publishers/twitter/streams/track/prod.json",:userpwd=> "jparekh@idyllic-software.com:idyllic",accept_encoding: 'gzip')    
  end

  def start_stream
    @client = Sidekiq::Client.new
    @request.on_headers do |response|
      if response.code != 200
        GnipStream.new.start_stream
        raise "Some Error On Stream"
      end
    end
    @request.on_body do |chunk|
      if chunk!="\r\n"
        while line = chunk.slice!(/.+\r?\n/)
          @client.push('queue'=>"gnip_stream",'class' => 'GnipStreamWorker', 'args' => [MultiJson.load(line)])
          # msg = { 'class' => 'GnipStreamWorker', 'args' => MultiJson.load(line), 'jid' => SecureRandom.hex(12), 'retry' => true }
          # redis.lpush("namespace:queue:gnip_stream_worker", JSON.dump(msg))
          # begin
          #   $redis.publish("one",MultiJson.load(line))
          # rescue Redis::BaseConnectionError => error
          #   puts "#{error}, retrying in 1s"
          # end  

          # puts MultiJson.load(line)
          # puts line
        end
      end  
    end
    @request.on_complete do |response|
      GnipStream.new.start_stream
      # Note that response.body is ""
    end
    @request.run
  end
end
GnipStream.new.start_stream

# request = Typhoeus::Request.new("https://stream.gnip.com/accounts/IdyllicSoftware/publishers/twitter/streams/track/prod.json",:userpwd=> "jparekh@idyllic-software.com:idyllic",accept_encoding: 'gzip')
 
# request.on_headers do |response|
#   if response.code != 200
#     binding.pry
#     raise "Request failed"
#   end
# end
# request.on_body do |chunk|
#   if chunk!="\r\n"

#     while line = chunk.slice!(/.+\r?\n/)

#       puts JSON.parse(line)
#       puts line
#     end
#   end    
# end
# request.on_complete do |response|
  
#   # Note that response.body is ""
# end
# request.run